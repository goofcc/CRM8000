package crm8000.viewModels

class WsBillVM {
	String id
	
	String ref
	
	String bizDate
	
	BigDecimal amount
	
	String customerName
	
	String warehouseName
	
	String inputUserName
	
	String bizUserName
	
	String billStatus
}
