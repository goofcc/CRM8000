package crm8000.viewModels

class InitInvertoryGoods {
	String id
	
	String code
	
	String name
	
	String spec
	
	String unit
	
	Integer count
	
	BigDecimal price
	
	BigDecimal money
}
