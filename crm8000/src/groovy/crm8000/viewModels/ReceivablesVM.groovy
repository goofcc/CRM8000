package crm8000.viewModels


class ReceivablesVM {
	String id
	
	String code
	
	String name
	
	String caId
	
	/**
	 * 应收总和
	 */
	BigDecimal rvMoney

	/**
	 * 实收总和
	 */
	BigDecimal actMoney
	
	/**
	 * 余额
	 */
	BigDecimal balanceMoney
}
