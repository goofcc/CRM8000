package crm8000.viewModels


class PayablesDetailVM {
	String id
	
	String refType
	
	String refNumber
	
	String bizDT
	
	/**
	 * 应付总和
	 */
	BigDecimal payMoney

	/**
	 * 实付总和
	 */
	BigDecimal actMoney
	
	/**
	 * 余额
	 */
	BigDecimal balanceMoney
}
