package crm8000.viewModels

class PwBillVM {
	String id
	
	String ref
	
	String bizDate
	
	BigDecimal amount
	
	String supplierName
	
	String warehouseName
	
	String inputUserName
	
	String bizUserName
	
	String billStatus
}
