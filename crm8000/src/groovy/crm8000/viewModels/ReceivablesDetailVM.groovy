package crm8000.viewModels

class ReceivablesDetailVM {
	String id
	
	String refType
	
	String refNumber
	
	String bizDT
	
	/**
	 * 应收总和
	 */
	BigDecimal rvMoney

	/**
	 * 实收总和
	 */
	BigDecimal actMoney
	
	/**
	 * 余额
	 */
	BigDecimal balanceMoney
}
