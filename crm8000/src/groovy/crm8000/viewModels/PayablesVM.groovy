package crm8000.viewModels


class PayablesVM {
	String id
	
	String code
	
	String name
	
	String caId
	
	/**
	 * 应付总和
	 */
	BigDecimal payMoney

	/**
	 * 实付总和
	 */
	BigDecimal actMoney
	
	/**
	 * 余额
	 */
	BigDecimal balanceMoney
}
