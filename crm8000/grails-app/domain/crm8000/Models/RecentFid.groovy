package crm8000.Models

class RecentFid {
	String fid
	
	String userId
	
	Date dateCreated
	
    static constraints = {
		dateCreated nullable: true
    }
	
	static mapping = {
		table 't_recent_fid'
	}
}
