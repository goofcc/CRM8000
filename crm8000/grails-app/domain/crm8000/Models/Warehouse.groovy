package crm8000.Models

/**
 * 仓库
 *
 */
class Warehouse {
	String id
	
	String code
	
	String name
	
	/***
	 * true: 已经完成库存建账
	 */
	Boolean inited

    static constraints = {
    }
	
	static mapping = {
		table 't_warehouse'
		
		id generator:'uuid.hex', params:[separator:'-']
	}
}
