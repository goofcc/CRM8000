package crm8000.Models

class Role {
	
	String id
	
	String name
	
	static hasMany = [permission: Permission, users: User]

    static constraints = {
    }
	
	static mapping = {
		table 't_role'
		id generator:'uuid.hex', params:[separator:'-']
		permission column: 'role_id', joinTable: 't_role_permission'
		users column: 'role_id', joinTable: 't_role_user' 
	}
}
