package crm8000.Models

class GoodsUnit {

	String id
	
	String name
	
    static constraints = {
    }
	
	static mapping = {
		table 't_goods_unit'
		
		id generator:'uuid.hex', params:[separator:'-']
	}
}
