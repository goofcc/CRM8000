package crm8000.Models

/**
 * 客户分类
 *
 */
class CustomerCategory {

	String id
	
	String code
	
	String name
	
    static constraints = {
    }
	
	static mapping = {
		table 't_customer_category'
		
		id generator:'uuid.hex', params:[separator:'-']
	}
}
