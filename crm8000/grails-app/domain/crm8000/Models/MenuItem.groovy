package crm8000.Models

class MenuItem {
	String id
	
	String parentId
	
	String caption
	
	int showOrder
	
	String fid
	
	ArrayList<MenuItem> children = new ArrayList<MenuItem>()

	static constraints = {
		parentId nullable: true
		fid nullable: true
    }
	
	static mapping = {
		table 't_menu_item'
		
		id generator:'uuid.hex', params:[separator:'-']
	}
	
	static transients = ['children']
}
