package crm8000.Models

/**
 * 供应商
 *
 */
class Supplier {
	String id
	
	SupplierCategory category
	
	String code
	
	String name
	
	String tel
	
	String qq

    static constraints = {
		tel nullable: true
		qq nullable: true
    }
	
	static mapping = {
		table 't_supplier'
		id generator:'uuid.hex', params:[separator:'-']
	}
}
