package crm8000.Models

import java.util.Date;

/**
 * 销售收款记录
 *
 */
class WsReceiving {
	String id
	
	BigDecimal actMoney
	
	/**
	 * 收款日期
	 */
	Date bizDate
	
	Date dateCreated
	
	WSBill wsBill
	
	/**
	 * 录单人
	 */
	User inputUser
	
	/**
	 * 收款人
	 */
	User rvUser
	
	/**
	 * 备注
	 */
	String remark
	
	static constraints = {
		dateCreated nullable: true
		remark nullabel: true
	}
	
	static mapping = {
		table 't_ws_receiving'
		
		wsBill column: 'wsbill_id'

		id generator:'uuid.hex', params:[separator:'-']
	}
}
