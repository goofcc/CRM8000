package crm8000.Models

/**
 * 销售退货入库单 - 主记录
 *
 */
class SRBill {
	String id

	String ref
	
	/**
	 * 对应的销售出库单
	 */
	WSBill wsBill

	Customer customer

	Warehouse warehouse

	User bizUser

	User inputUser

	Date bizDT

	Date dateCreated

	Integer billStatus

	/**
	 * 退款
	 */
	BigDecimal retrunSaleMoney

	/**
	 * 存货成本
	 */
	BigDecimal invertoryMoney

	/**
	 * 减少的毛利: = retrunSaleMoney - invertoryMoney
	 */
	BigDecimal profit
	
	static hasMany = [details: SRBillDetail]

	static constraints = {
		dateCreated nullable: true
		retrunSaleMoney nullable: true
		invertoryMoney nullable: true
		profit nullable: true
	}

	static mapping = {
		table 't_sr_bill'

		id generator:'uuid.hex', params:[separator:'-']
	}
}