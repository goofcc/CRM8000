package crm8000.Models

/**
 * 应收账款，按客户汇总
 *
 */
class Receivables {
	String id
	
	/**
	 * 往来单位的Id，可能是客户Id或者是供应商Id
	 */
	String caId
	
	/**
	 * 往来单位的类型，值为：customer/supplier
	 */
	String caType

	/**
	 * 应收总和	
	 */
	BigDecimal rvMoney

	/**
	 * 实收总和
	 */
	BigDecimal actMoney
	
	/**
	 * 余额
	 */
	BigDecimal balanceMoney
	
    static constraints = {
    }
	
	static mapping = {
		table 't_receivables'

		id generator:'uuid.hex', params:[separator:'-']
	}
}
