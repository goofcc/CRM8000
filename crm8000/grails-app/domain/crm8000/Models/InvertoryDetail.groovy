package crm8000.Models

/**
 * 库存明细账 
 *
 */
class InvertoryDetail {
	Warehouse warehouse
	
	Goods goods
	
	Integer inCount
	
	BigDecimal inPrice
	
	BigDecimal inMoney
	
	Integer outCount
	
	BigDecimal outPrice
	
	BigDecimal outMoney
	
	Integer balanceCount
	
	BigDecimal balancePrice
	
	BigDecimal balanceMoney
	
	Date dateCreated
	
	String refType
	
	String refNumber
	
	Date bizDate
	
	User bizUser

    static constraints = {
		inCount nullable: true
		inPrice nullable: true
		inMoney nullable: true
		outCount nullable: true
		outPrice nullable: true
		outMoney nullable: true
		dateCreated nullable: true
		refNumber nullable: true
    }
	
	static mapping = {
		table 't_invertory_detail'
	}
}
