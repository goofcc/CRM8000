package crm8000.Models

class CustomerCodeHelp {
	
	String customerId
	
	String codeHelp
	
	static constraints = {
	}
	
	static mapping = {
		table 't_customer_code_help'
		version false
	}
}