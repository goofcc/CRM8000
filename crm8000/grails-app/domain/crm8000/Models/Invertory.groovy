package crm8000.Models

/**
 * 库存总账
 *
 */
class Invertory {
	Warehouse warehouse
	
	Goods goods
	
	Integer inCount
	
	BigDecimal inPrice
	
	BigDecimal inMoney
	
	Integer outCount
	
	BigDecimal outPrice
	
	BigDecimal outMoney
	
	Integer balanceCount
	
	BigDecimal balancePrice
	
	BigDecimal balanceMoney

    static constraints = {
		inCount nullable: true
		inPrice nullable: true
		inMoney nullable: true
		outCount nullable: true
		outPrice nullable: true
		outMoney nullable: true
    }
	
	static mapping = {
		table 't_invertory'
	}
}
