package crm8000.Models

import java.util.Date;

/**
 * 销售出库单 - 主记录
 *
 */
class WSBill {
	String id
	
	String ref
	
	Customer customer
	
	Warehouse warehouse
	
	User bizUser
	
	User inputUser
	
	Date bizDT
	
	Date dateCreated
	
	Integer billStatus
	
	BigDecimal saleMoney
	
	BigDecimal invertoryMoney
	
	BigDecimal profit
	
	static hasMany = [details: WSBillDetail]

	static constraints = {
		dateCreated nullable: true
		saleMoney nullable: true
		invertoryMoney nullable: true
		profit nullable: true
	}
	
	static mapping = {
		table 't_ws_bill'
		
		id generator:'uuid.hex', params:[separator:'-']
	}
}