package crm8000.Models

/**
 * 用户
 *
 */
class User {
	String id
	
	String loginName
	
	String name
	
	String password
	
	Org org
	
	String orgCode
	
	Boolean enabled

    static constraints = {
    }
	
	static mapping = {
		table 't_user'
		id generator:'uuid.hex', params:[separator:'-']
	}
}
