package crm8000.Models

import java.util.Date;

/**
 * 销售出库单 - 商品明细记录
 *
 */
class WSBillDetail {
	String id

	WSBill wsBill

	Goods goods

	Integer goodsCount

	BigDecimal goodsMoney

	BigDecimal goodsPrice

	Date dateCreated

	Integer showOrder

	BigDecimal invertoryMoney

	BigDecimal invertoryPrice

	static belongsTo = WSBill

	static constraints = {
		dateCreated nullable: true
		invertoryMoney nullable: true
		invertoryPrice nullable: true
	}

	static mapping = {
		table 't_ws_bill_detail'

		id generator:'uuid.hex', params:[separator:'-']

		wsBill column: 'wsbill_id'
	}
}
