package crm8000.Models

class GoodsCategory {

	String id
	
	String code
	
	String name
	
    static constraints = {
    }
	
	static mapping = {
		table 't_goods_category'
		
		id generator:'uuid.hex', params:[separator:'-']
	}
}
