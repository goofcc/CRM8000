package crm8000.Models


/**
 * 采购退货出库单 - 明细记录
 *
 */
class PRBillDetail {
	String id

	PRBill prBill

	Goods goods

	Integer goodsCount

	BigDecimal goodsMoney

	BigDecimal goodsPrice

	Date dateCreated

	Integer showOrder
	
	PWBillDetail pwBillDetail

	static belongsTo = PRBill

	static constraints = { dateCreated nullable: true }

	static mapping = {
		table 't_pr_bill_detail'

		id generator:'uuid.hex', params:[separator:'-']

		prBill column: 'prbill_id'
		
		pwBillDetail column: 'pwbilldetail_id'
	}
}
