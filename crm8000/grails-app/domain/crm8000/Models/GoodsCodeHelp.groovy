package crm8000.Models

class GoodsCodeHelp {

	String goodsId
	
	String codeHelp
	
    static constraints = {
    }
	
	static mapping = {
		table 't_goods_code_help'
		version false
	}
}
