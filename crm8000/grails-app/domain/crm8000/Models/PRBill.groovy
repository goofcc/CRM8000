package crm8000.Models

import java.util.Date;

/**
 * 采购退货出库单 - 主记录
 *
 */
class PRBill {
	String id
	
	String ref
	
	Supplier supplier
	
	Warehouse warehouse
	
	User bizUser
	
	User inputUser
	
	Date bizDT
	
	Date dateCreated
	
	Integer billStatus
	
	BigDecimal goodsMoney
	
	PWBill pwBill
	
	static hasMany = [details: PRBillDetail]

	static constraints = {
		dateCreated nullable: true
	}
	
	static mapping = {
		table 't_pr_bill'
		
		id generator:'uuid.hex', params:[separator:'-']
		
		pwBill column: 'pwbill_id'
	}
}
