package crm8000.Models

/**
 * 客户资料
 *
 */
class Customer {
	String id

	CustomerCategory category

	String code

	String name

	String tel

	String qq

	static constraints = {
		tel nullable: true
		qq nullable: true
	}

	static mapping = {
		table 't_customer'

		id generator:'uuid.hex', params:[separator:'-']
	}
}
