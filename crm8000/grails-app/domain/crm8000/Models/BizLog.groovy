package crm8000.Models

/**
 * 业务日志
 *
 */
class BizLog {

	User user
	
	String ip
	
	String info
	
	Date dateCreated
	
    static constraints = {
		dateCreated nullable: true
    }
	
	static mapping = {
		table 't_biz_log'
		version false
	}
}
