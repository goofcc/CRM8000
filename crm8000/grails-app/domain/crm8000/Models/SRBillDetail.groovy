package crm8000.Models

/**
 * 销售退货入库单 - 明细记录
 */
class SRBillDetail {
	String id

	SRBill srBill

	Goods goods

	/**
	 * 退货数量
	 */
	Integer goodsCount

	/**
	 * 销售金额
	 */
	BigDecimal goodsMoney

	/**
	 * 退款。
	 * 退款通常等于销售金额，也有不等同的时候。
	 */
	BigDecimal retrunSaleMoney
	
	/**
	 * 销售单价
	 */
	BigDecimal goodsPrice

	Date dateCreated

	Integer showOrder

	BigDecimal invertoryMoney

	BigDecimal invertoryPrice
	
	WSBillDetail wsBillDetail

	static belongsTo = SRBill

	static constraints = {
		dateCreated nullable: true
	}

	static mapping = {
		table 't_sr_bill_detail'

		id generator:'uuid.hex', params:[separator:'-']

		srBill column: 'srbill_id'
		
		wsBillDetail column: 'wsbilldetail_id'
	}
}