package crm8000.Models

/**
 * 权限
 *
 */
class Permission {

	String id
	
	String name
	
	String fid
	
	String note
	
    static constraints = {
		note nullable: true
    }
	
	static mapping = {
		table 't_permission'
		id generator:'uuid.hex', params:[separator:'-']
	}
}
