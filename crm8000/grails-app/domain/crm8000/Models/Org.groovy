package crm8000.Models

class Org {
	String id
	
	String name
	
	String fullName
	
	String parentId
	
	String orgCode
	
	Boolean leaf
	
	String parentOrgName
	
	ArrayList<Org> children = new ArrayList<Org>()

	static constraints = {
		parentId nullable: true
	}

	static mapping = {
		table 't_org'
		id generator:'uuid.hex', params:[separator:'-']
	}
	
	static transients = ['leaf', 'children', 'parentOrgName']
}
