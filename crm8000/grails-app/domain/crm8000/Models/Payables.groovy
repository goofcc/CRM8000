package crm8000.Models


/**
 * 应付账款，按供应商汇总
 *
 */
class Payables {
	String id
	
	/**
	 * 往来单位的Id，可能是客户Id或者是供应商Id
	 */
	String caId
	
	/**
	 * 往来单位的类型，值为：customer/supplier
	 */
	String caType

	/**
	 * 应付总和
	 */
	BigDecimal payMoney

	/**
	 * 实付总和
	 */
	BigDecimal actMoney
	
	/**
	 * 余额
	 */
	BigDecimal balanceMoney
	
	static constraints = {
	}
	
	static mapping = {
		table 't_payables'

		id generator:'uuid.hex', params:[separator:'-']
	}
}
