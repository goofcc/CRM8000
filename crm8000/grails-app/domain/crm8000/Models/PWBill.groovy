package crm8000.Models

import java.util.Date;

/**
 * 采购入库单 - 主记录
 *
 */
class PWBill {
	String id
	
	String ref
	
	Supplier supplier
	
	Warehouse warehouse
	
	User bizUser
	
	User inputUser
	
	Date bizDT
	
	Date dateCreated
	
	Integer billStatus
	
	BigDecimal goodsMoney
	
	static hasMany = [details: PWBillDetail]

    static constraints = {
		dateCreated nullable: true
    }
	
	static mapping = {
		table 't_pw_bill'
		
		id generator:'uuid.hex', params:[separator:'-']
	}
}
