package crm8000.Models

class SupplierCodeHelp {
	
	String supplierId
	
	String codeHelp

    static constraints = {
    }
	
	static mapping = {
		table 't_supplier_code_help'
		version false
	}
}
