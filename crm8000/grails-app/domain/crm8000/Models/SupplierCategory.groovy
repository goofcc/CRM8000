package crm8000.Models

/**
 * 供应商分类
 *
 */
class SupplierCategory {
	String id
	
	String code
	
	String name

    static constraints = {
    }
	
	static mapping = {
		table 't_supplier_category'
		id generator:'uuid.hex', params:[separator:'-']
	}
}
