package crm8000.Models

/**
 * 商品
 *
 */
class Goods {
	String id
	
	GoodsCategory category
	
	String code
	
	String name
	
	String spec
	
	GoodsUnit unit
	
	BigDecimal salePrice
	
    static constraints = {
    }
	
	static mapping = {
		table 't_goods'
		
		id generator:'uuid.hex', params:[separator:'-']
	}
}
