package crm8000.Models;

import java.util.Date;

/**
 * 应收账款 - 明细账，按单据汇总
 *
 */
class ReceivablesDetail {
	String id
	
	/**
	 * 往来单位的Id，可能是客户Id或者是供应商Id
	 */
	String caId
	
	/**
	 * 往来单位的类型，值为：customer/supplier
	 */
	String caType
	
	/**
	 * 应收总和
	 */
	BigDecimal rvMoney
	
	/**
	 * 实收总和
	 */
	BigDecimal actMoney
	
	/**
	 * 余额
	 */
	BigDecimal balanceMoney
	
	Date dateCreated
	
	String refType
	
	String refNumber
	
    static constraints = {
		dateCreated nullable: true
    }
	
	static mapping = {
		table 't_receivables_detail'

		id generator:'uuid.hex', params:[separator:'-']
	}
}
