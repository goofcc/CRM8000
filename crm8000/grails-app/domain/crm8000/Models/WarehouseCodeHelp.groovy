package crm8000.Models

class WarehouseCodeHelp {
	String warehouseId
	
	String codeHelp

    static constraints = {
    }
	
	static mapping = {
		table 't_warehouse_code_help'
		version false
	}
}
