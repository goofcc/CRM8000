package crm8000.Models

import java.util.Date;

/**
 * 采购入库的付款的记录
 *
 */
class PwPayment {
	String id
	
	/**
	 * 付款金额
	 */
	BigDecimal actMoney
	
	/**
	 * 付款日期
	 */
	Date bizDate
	
	Date dateCreated
	
	PWBill pwBill
	
	/**
	 * 录单人
	 */
	User inputUser
	
	/**
	 * 付款人
	 */
	User payUser
	
	/**
	 * 备注
	 */
	String remark
	
	static constraints = {
		dateCreated nullable: true
		remark nullabel: true
	}
	
	static mapping = {
		table 't_pw_payment'
		
		pwBill column: 'pwbill_id'

		id generator:'uuid.hex', params:[separator:'-']
	}
}
