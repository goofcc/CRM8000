package crm8000.Models

import java.util.Date;

/**
 * 采购入库单 - 明细记录
 *
 */
class PWBillDetail {
	String id

	PWBill pwBill
	
	Goods goods
	
	Integer goodsCount
	
	BigDecimal goodsMoney
	
	BigDecimal goodsPrice
	
	Date dateCreated
	
	Integer showOrder
	
	static belongsTo = PWBill
	
    static constraints = {
		dateCreated nullable: true
    }
	
	static mapping = {
		table 't_pw_bill_detail'
		
		id generator:'uuid.hex', params:[separator:'-']
		
		pwBill column: 'pwbill_id'
	}
}
