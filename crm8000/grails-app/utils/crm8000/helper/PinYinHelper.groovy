package crm8000.helper

class PinYinHelper {

	def toPinYin(String chinese) {
		def result = []

		for (int i = 0; i < chinese.length(); i++){
			def word = chinese.charAt(i)
			def py = net.sourceforge.pinyin4j.PinyinHelper.toHanyuPinyinStringArray(word)
			def pyList = []
			if (py == null) {
				pyList.add(word.toString())
			} else {
				for (p in py) {
					def first = p.substring(0, 1)
					if (!pyList.contains(first)) {
						pyList.add(first)
					}
				}
			}

			if (i == 0) {
				for (p in pyList) {
					result.add(p)
				}
			} else {
				def temp = []
				for (r in result) {
					for (p in pyList) {
						temp.add(r + p)
					}
				}
				result = temp
			}
		}

		return result
	}
}
