<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>修改我的密码 - CRM8000</title>
	</head>
	<body>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/User/ChangeMyPasswordForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        var mainForm = Ext.create("CRM8000.User.ChangeMyPasswordForm", {
	            loginUserId: "${loginUserId}",
	            loginUserName: "${loginName}",
	            loginUserFullName: "${loginUserName}",
	            baseURL: "${createLink(uri: '/')}"
	        });
	
	        app.add(mainForm);
	
	        app.setAppHeader({
	            title: "修改我的密码",
	            iconCls: "CRM8000-fid-9996"
	        });
	    });
	</script>
	</body>
</html>
