<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>用户管理 - CRM8000</title>
	</head>
	<body>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/User/MainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/User/OrgEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/User/ParentOrgEditor.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/User/UserEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/User/OrgEditor.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/User/ChangeUserPasswordForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });

	        app.add(Ext.create("CRM8000.User.MainForm"));
	
	        app.setAppHeader({
	            title: "用户管理",
	            iconCls: "CRM8000-fid-8999"
	        });
	    });
	</script>
	</body>
</html>
