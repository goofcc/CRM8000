<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="layoutLogin">
		<title>登录</title>
	</head>
	<body>
	<style type="text/css">
	    #loading-mask {
	        background-color: white;
	        height: 100%;
	        position: absolute;
	        left: 0;
	        top: 0;
	        width: 100%;
	        z-index: 20000;
	    }
	
	    #loading {
	        height: auto;
	        position: absolute;
	        left: 45%;
	        top: 40%;
	        padding: 2px;
	        z-index: 20001;
	    }
	
	    #loading .loading-indicator {
	        background: white;
	        color: #444;
	        font: bold 13px Helvetica, Arial, sans-serif;
	        height: auto;
	        margin: 0;
	        padding: 10px;
	    }
	
	    #loading-msg {
	        font-size: 10px;
	        font-weight: normal;
	    }
	</style>
	
	<div id="loading-mask" style=""></div>
	<div id="loading">
	    <div class="loading-indicator">
	        <img src="${resource(dir: 'Images', file: 'loader.gif')}" width="32" height="32" style="margin-right: 8px; float: left; vertical-align: top;" />
	        欢迎使用CRM8000
	        <br />
	        <span id="loading-msg">加载中...</span>
	    </div>
	</div>
	
	<link href="${resource(dir: 'ExtJS/resources/css', file: 'ext-all.css')}" rel="stylesheet" type="text/css"/>

	<link href="${resource(dir: 'Content', file: 'icons.css')}" rel="stylesheet" type="text/css"/>
	<link href="${resource(dir: 'Content', file: 'Site.css')}" rel="stylesheet" type="text/css"/>

    <script src="${resource(dir: 'ExtJS', file: 'ext-all.js')}" type="text/javascript"></script>
    <script src="${resource(dir: 'ExtJS', file: 'ext-lang-zh_CN.js')}" type="text/javascript"></script>
    <script src="${resource(dir: 'Scripts/CRM8000', file: 'MsgBox.js')}" type="text/javascript"></script>
    <script src="${resource(dir: 'Scripts/CRM8000', file: 'Const.js')}" type="text/javascript"></script>
    <script src="${resource(dir: 'Scripts/CRM8000/User', file: 'LoginForm.js')}" type="text/javascript"></script>
	
	<script type="text/javascript">
	    Ext.onReady(function () {
	        var form = Ext.create("CRM8000.User.LoginForm", {
					baseURL: "${createLink(uri: '/')}"
		        });
	        form.show();
	
	        Ext.get("loading").remove();
	        Ext.get("loading-mask").remove();
	    });
	</script>
	
	</body>
</html>
