<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>销售出库 - CRM8000</title>
	</head>
	<body>

	<script src="${createLink(uri: '/')}Scripts/CRM8000/Warehouse/WarehouseField.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/User/UserField.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Customer/CustomerField.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Goods/GoodsWithSalePriceField.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script src="${createLink(uri: '/')}Scripts/CRM8000/Sale/WSMainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Sale/WSEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Sale/WSDetailEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Sale.WSMainForm"));

	        app.setAppHeader({
	        	title: "销售出库",
	            iconCls: "CRM8000-fid2002"
	        });
	    });
	</script>
	
	</body>
</html>
