<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>销售退货入库 - CRM8000</title>
	</head>
	<body>

	<script src="${createLink(uri: '/')}Scripts/CRM8000/Sale/SRMainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });

	        app.add(Ext.create("CRM8000.Sale.SRMainForm"));

	        app.setAppHeader({
	        	title: "销售退货入库",
	            iconCls: "CRM8000-fid2006"
	        });
	    });
	</script>
	
	</body>
</html>
