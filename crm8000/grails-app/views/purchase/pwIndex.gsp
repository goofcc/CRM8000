<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>采购入库 - CRM8000</title>
	</head>
	<body>

	<script src="${createLink(uri: '/')}Scripts/CRM8000/Warehouse/WarehouseField.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/User/UserField.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Supplier/SupplierField.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Goods/GoodsField.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script src="${createLink(uri: '/')}Scripts/CRM8000/Purchase/PWMainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Purchase/PWEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Purchase/PWDetailEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Purchase.PWMainForm"));

	        app.setAppHeader({
	        	title: "采购入库",
	            iconCls: "CRM8000-fid2001"
	        });
	    });
	</script>
	
	</body>
</html>
