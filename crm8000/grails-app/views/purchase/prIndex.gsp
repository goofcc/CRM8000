<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>采购退货出库 - CRM8000</title>
	</head>
	<body>

	<script src="${createLink(uri: '/')}Scripts/CRM8000/Purchase/PRMainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });

	        app.add(Ext.create("CRM8000.Purchase.PRMainForm"));

	        app.setAppHeader({
	        	title: "采购退货出库",
	            iconCls: "CRM8000-fid2007"
	        });
	    });
	</script>
	
	</body>
</html>
