<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>客户资料 - CRM8000</title>
	</head>
	<body>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Customer/MainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Customer/CategoryEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Customer/CustomerEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Customer.MainForm"));

	        app.setAppHeader({
	            title: "客户资料",
	            iconCls: "CRM8000-fid1007"
	        });
	    });
	</script>
	
	</body>
</html>
