<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>应付账款管理 - CRM8000</title>
	</head>
	<body>

	<script src="${createLink(uri: '/')}Scripts/CRM8000/User/UserField.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script src="${createLink(uri: '/')}Scripts/CRM8000/Funds/PayMainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Funds/PaymentEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Funds.PayMainForm"));

	        app.setAppHeader({
	            title: "应付账款管理",
	            iconCls: "CRM8000-fid2005"
	        });
	    });
	</script>
	
	</body>
</html>
