<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>供应商档案 - CRM8000</title>
	</head>
	<body>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Supplier/MainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Supplier/CategoryEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Supplier/SupplierEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Supplier.MainForm"));

	        app.setAppHeader({
	            title: "供应商档案",
	            iconCls: "CRM8000-fid1004"
	        });
	    });
	</script>
	
	</body>
</html>
