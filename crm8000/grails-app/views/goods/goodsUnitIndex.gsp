<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>商品计量单位 - CRM8000</title>
	</head>
	<body>
	
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Goods/UnitMainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Goods/UnitEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Goods.UnitMainForm", {
	        	baseURL: "${createLink(uri: '/')}"
			}));
	
	        app.setAppHeader({
	            title: "商品计量单位",
	            iconCls: "CRM8000-fid1002"
	        });
	    });
	</script>
	
	</body>
</html>
