<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>商品 - CRM8000</title>
	</head>
	<body>

	<script src="${createLink(uri: '/')}Scripts/CRM8000/Goods/MainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Goods/CategoryEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Goods/GoodsEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Goods.MainForm"));

	        app.setAppHeader({
	            title: "商品",
	            iconCls: "CRM8000-fid1001"
	        });
	    });
	</script>
	
	</body>
</html>
