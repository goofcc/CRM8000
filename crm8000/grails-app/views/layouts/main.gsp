<!DOCTYPE html>
<html lang="zh">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8" />
		<title><g:layoutTitle default="CRM8000" /></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}"
			type="image/x-icon">

  		<link href="${resource(dir: 'ExtJS/resources/css', file: 'ext-all.css')}" rel="stylesheet" type="text/css"/>

  		<link href="${resource(dir: 'Content', file: 'icons.css')}" rel="stylesheet" type="text/css"/>
  		<link href="${resource(dir: 'Content', file: 'Site.css')}" rel="stylesheet" type="text/css"/>

	    <script src="${resource(dir: 'ExtJS', file: 'ext-all.js')}" type="text/javascript"></script>
	    <script src="${resource(dir: 'ExtJS', file: 'ext-lang-zh_CN.js')}" type="text/javascript"></script>
	    <script src="${resource(dir: 'Scripts/CRM8000', file: 'MsgBox.js')}" type="text/javascript"></script>
	    <script src="${resource(dir: 'Scripts/CRM8000', file: 'Const.js')}" type="text/javascript"></script>
	    <script src="${resource(dir: 'Scripts/CRM8000', file: 'App.js')}" type="text/javascript"></script>
		<g:layoutHead />
	</head>
	<body>
		<script type="text/javascript">
			CRM8000.Const.BASE_URL = "${createLink(uri: '/')}";
		</script>
		<g:layoutBody />
	</body>
</html>
