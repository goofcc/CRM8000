<!DOCTYPE html>
<html lang="zh">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8" />
		<title><g:layoutTitle default="CRM8000" /></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}"
			type="image/x-icon">
		<g:layoutHead />
	</head>
	<body>
		<g:layoutBody />
	</body>
</html>
