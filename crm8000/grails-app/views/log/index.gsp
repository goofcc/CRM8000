<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>系统日志 - CRM8000</title>
	</head>
	<body>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Log/MainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Log.MainForm"));

	        app.setAppHeader({
	            title: "系统日志",
	            iconCls: "CRM8000-fid-8997"
	        });
	    });
	</script>
	
	</body>
</html>
