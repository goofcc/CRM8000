<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>首页 - CRM8000</title>
	</head>
	<body>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Home/MainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Home.MainForm"));
	        app.setAppHeader({
	            title: "首页",
	            iconCls: "CRM8000-fid-9997"
	            });
	    });
	</script>
	</body>
</html>
