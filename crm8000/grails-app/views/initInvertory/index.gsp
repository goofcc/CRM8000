<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>库存建账 - CRM8000</title>
	</head>
	<body>

	<script src="${createLink(uri: '/')}Scripts/CRM8000/Invertory/InitInvMainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Invertory/InitInvEditForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Invertory.InitInvMainForm"));

	        app.setAppHeader({
	        	title: "库存建账",
	            iconCls: "CRM8000-fid2000"
	        });
	    });
	</script>
	
	</body>
</html>