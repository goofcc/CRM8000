<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>仓库 - CRM8000</title>
	</head>
	<body>
	<script src="${createLink(uri: '/')}Scripts/CRM8000/Invertory/InvQueryMainForm.js?dt=${System.currentTimeMillis()}" type="text/javascript"></script>

	<script type="text/javascript">
	    Ext.onReady(function () {
	        var app = Ext.create("CRM8000.App", {
	            userName: "${loginUserName}"
	        });
	
	        app.add(Ext.create("CRM8000.Invertory.InvQueryMainForm"));

	        app.setAppHeader({
	        	title: "库存账查询",
	            iconCls: "CRM8000-fid2003"
	        });
	    });
	</script>
	
	</body>
</html>
