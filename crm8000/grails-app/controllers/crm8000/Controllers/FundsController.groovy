package crm8000.Controllers

import java.awt.event.ItemEvent;

import crm8000.services.PayService
import crm8000.services.PermissionService;
import crm8000.services.RvService
import crm8000.services.UserService;
import grails.converters.JSON

class FundsController {
	static allowedMethods = [rvCategoryList: 'POST', rvList: 'POST',
		rvDetailList: 'POST', rvRecordList: 'POST', rvRecInfo: 'POST',
		addRvRecord: 'POST', refreshRvInfo: 'POST', refreshRvDetailInfo: 'POST',
		payCategoryList: 'POST', payList: "POST", payDetailList: "POST", 
		payRecordList: "POST", addPayment: "POST", refreshPayInfo: "POST",
		refreshPayDetailInfo: "POST"]

	PermissionService permissionService

	UserService userService

	RvService rvService

	PayService payService

	def rvIndex() {
		if (!permissionService.hasPermission('2004')) {
			redirect controller: 'home'
		}

		[loginUserName: userService.loginUserName]
	}

	def payIndex() {
		if (!permissionService.hasPermission('2005')) {
			redirect controller: 'home'
		}

		[loginUserName: userService.loginUserName]
	}

	def rvCategoryList() {
		def data = rvService.categoryList(params.id)

		render(contentType: "application/json") {
			array {
				for (c in data) {
					category id: c.id, name: c.code + " - "+ c.name
				}
			}
		}
	}

	def rvList() {
		def data = rvService.rvList(params)
		def tc = rvService.rvListTotalCount(params)

		render(contentType: "application/json") {
			totalCount = tc
			dataList = array {
				for (item in data) {
					rv id: item.id, code: item.code, name: item.name,
					rvMoney: item.rvMoney, actMoney: item.actMoney,
					balanceMoney: item.balanceMoney, caId: item.caId
				}
			}
		}
	}

	def rvDetailList() {
		def data = rvService.rvDetailList(params)
		def tc = rvService.rvDetailListTotalCount(params)

		render(contentType: "application/json") {
			totalCount = tc
			dataList = array {
				for (item in data) {
					rvDetail id: item.id, refType: item.refType, refNumber: item.refNumber,
					rvMoney: item.rvMoney, actMoney: item.actMoney,
					balanceMoney: item.balanceMoney, bizDT: item.bizDT
				}
			}
		}
	}

	def rvRecordList() {
		def data = rvService.rvRecordList(params)
		def tc = rvService.rvRecordListTotalCount(params)

		render(contentType: "application/json") {
			totalCount = tc
			dataList = array {
				for (item in data) {
					rvRecord bizDate: item.bizDate.format("yyyy-MM-dd"), actMoney: item.actMoney,
					bizUserName: item.rvUser.name, inputUserName: item.inputUser.name,
					remark: item.remark, dateCreated: item.dateCreated.format("yyyy-MM-dd HH:mm")
				}
			}
		}
	}

	def rvRecInfo() {
		def user = userService.loginUser
		render(contentType: "application/json") {
			bizUserId = user.id
			bizUserName = user.name
		}
	}

	def addRvRecord() {
		def result = rvService.addRvRecord(params)

		render result as JSON
	}

	def refreshRvInfo() {
		def rv = rvService.refreshRvInfo(params.id)
		render(contentType: "application/json") {
			actMoney = rv?.actMoney
			balanceMoney = rv?.balanceMoney
		}
	}

	def refreshRvDetailInfo() {
		def rvDetail = rvService.refreshRvDetailInfo(params.id)
		
		render(contentType: "application/json") {
			actMoney = rvDetail?.actMoney
			balanceMoney = rvDetail?.balanceMoney
		}
	}

	def payCategoryList() {
		def data = payService.categoryList(params.id)

		render(contentType: "application/json") {
			array {
				for (c in data) {
					category id: c.id, name: c.code + " - "+ c.name
				}
			}
		}
	}
	
	def payList() {
		def data = payService.payList(params)
		def tc = payService.payListTotalCount(params)

		render(contentType: "application/json") {
			totalCount = tc
			dataList = array {
				for (item in data) {
					pay id: item.id, code: item.code, name: item.name,
					payMoney: item.payMoney, actMoney: item.actMoney,
					balanceMoney: item.balanceMoney, caId: item.caId
				}
			}
		}
	}
	
	def payDetailList() {
		def data = payService.payDetailList(params)
		def tc = payService.payDetailListTotalCount(params)

		render(contentType: "application/json") {
			totalCount = tc
			dataList = array {
				for (item in data) {
					payDetail id: item.id, refType: item.refType, refNumber: item.refNumber,
					payMoney: item.payMoney, actMoney: item.actMoney,
					balanceMoney: item.balanceMoney, bizDT: item.bizDT
				}
			}
		}
	}
	
	def payRecordList() {
		def data = payService.payRecordList(params)
		def tc = payService.payRecordListTotalCount(params)

		render(contentType: "application/json") {
			totalCount = tc
			dataList = array {
				for (item in data) {
					payRecord bizDate: item.bizDate.format("yyyy-MM-dd"), actMoney: item.actMoney,
					bizUserName: item.payUser.name, inputUserName: item.inputUser.name,
					remark: item.remark, dateCreated: item.dateCreated.format("yyyy-MM-dd HH:mm")
				}
			}
		}
	}
	
	def payRecInfo() {
		def user = userService.loginUser
		render(contentType: "application/json") {
			bizUserId = user.id
			bizUserName = user.name
		}
	}
	
	def addPayment() {
		def result = payService.addPayment(params)
		
		render result as JSON
	}
	
	def refreshPayInfo() {
		def pay = payService.refreshPayInfo(params.id)
		render(contentType: "application/json") {
			actMoney = pay?.actMoney
			balanceMoney = pay?.balanceMoney
		}
	}
	
	def refreshPayDetailInfo() {
		def payDetail = payService.refreshPayDetailInfo(params.id)
		
		render(contentType: "application/json") {
			actMoney = payDetail?.actMoney
			balanceMoney = payDetail?.balanceMoney
		}
	}
}
