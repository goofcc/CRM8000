package crm8000.Controllers

import crm8000.services.BizLogService
import crm8000.services.PermissionService
import crm8000.services.UserService

class LogController {
	
	static allowedMethods = [logList: 'POST']

	PermissionService permissionService

	UserService userService

	BizLogService bizLogService

	def index() {
		if (!permissionService.hasPermission('-8997')) {
			redirect controller: 'home'
		}

		[loginUserName: userService.loginUserName]
	}

	def logList() {
		def data = bizLogService.logList(params)
		def count = bizLogService.logListCount()

		render(contentType: "application/json") {
			logs = array{
				for (item in data) {
					node id: item.id, loginName: item.user.loginName, userName: item.user.name,
					ip: item.ip, content: item.info, 
					dt: item.dateCreated?.format("yyyy-MM-dd HH:mm:ss")
				}
			}

			totalCount = count
		}
	}
}
