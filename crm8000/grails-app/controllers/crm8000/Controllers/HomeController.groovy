package crm8000.Controllers

import crm8000.Models.FId;
import crm8000.Models.MenuItem;
import crm8000.services.BizLogService;
import crm8000.services.FidService
import crm8000.services.MainMenuService
import crm8000.services.RecentFidService;
import crm8000.services.UserService;
import grails.converters.JSON

class HomeController {

	static allowedMethods = [mainMenuItems: 'POST', recentFid: 'POST']

	def MainMenuService mainMenuService

	def FidService fidService

	def UserService userService

	def RecentFidService recentFidService

	def BizLogService bizLogService

	def index() {
		[loginUserName: userService.loginUserName]
	}

	def error() { }

	def navigateTo() {
		def fid = params.fid
		if (!fid) {
			redirect controller: "home"
		}

		recentFidService.insertRecentFid(fid)
		if (fid != "-9999") {
			def f = FId.findByFid(fid)
			if (f)
				bizLogService.log("进入模块：" + f.name)
		}

		switch(fid) {
			case "-9999":
			// 重新登录
				userService.clearLoginUserInSession()
				bizLogService.log("退出系统准备重新登录")
				redirect controller: "home"
				break
			case "-9996":
			// 修改我的密码
				redirect controller: "user", action: "changeMyPassword"
				break
			case "-8999":
			// 用户管理
				redirect controller: "user"
				break
			case "-8997":
			// 系统日志
				redirect controller: "log"
				break
			case "-8996":
			// 权限管理
				redirect controller: 'permission'
				break
			case "1001":
			// 商品
				redirect controller: "goods"
				break;
			case "1002":
			// 商品计量单位
				redirect controller: "goods", action: "goodsUnitIndex"
				break
			case "1003":
			// 仓库
				redirect controller: "warehouse"
				break
			case "1004":
			// 供应商档案
				redirect controller: "supplier"
				break
			case "1007":
			// 客户资料
				redirect controller: "customer"
				break
			case "2000":
			// 库存建账
				redirect controller: "initInvertory"
				break
			case "2001":
			// 采购入库
				redirect controller: "purchase", action: "pwIndex"
				break
			case "2002":
			// 销售出库
				redirect controller: "sale", action: "wsIndex"
				break
			case "2003":
			// 库存账查询
				redirect controller: "invertory", action: "invQueryIndex"
				break
			case "2004":
			// 应收账款管理
				redirect controller: "funds", action: "rvIndex"
				break
			case "2005":
			// 应付账款管理
				redirect controller: "funds", action: "payIndex"
				break
			case "2006":
			// 销售退货
				redirect controller: "sale", action: "srIndex"
				break;
			case "2007":
			// 采购退货
				redirect controller: "purchase", action: "prIndex"
				break;
			default:
				redirect controller: "home"
		}
	}

	def mainMenuItems() {
		def result = mainMenuService.getMainMenuItems()

		render(contentType: "application/json") {
			array {
				for (m1 in result.children) {
					menu1 caption: m1.caption, fid: m1.fid,
					children:  array {
						for (m2 in m1.children) {
							menu2 caption: m2.caption, fid: m2.fid,
							children: array {
								for (m3 in m2.children) {
									menu3 caption: m3.caption, fid: m3.fid, children: array {}
								}
							}
						}
					}
				}
			}
		}
	}

	def recentFid() {
		def result = fidService.recentFid()

		render(contentType: "application/json") {
			root = array {
				for (r in result){
					fid(id: r.fid, name: r.name)
				}
			}
		}
	}
}
