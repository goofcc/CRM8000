package crm8000.Controllers

import crm8000.Models.FId;
import crm8000.Models.Goods;
import crm8000.Models.GoodsUnit;
import crm8000.services.BizLogService;
import crm8000.services.FidService
import crm8000.services.GoodsService
import crm8000.services.MainMenuService
import crm8000.services.PermissionService
import crm8000.services.RecentFidService;
import crm8000.services.UserService;
import grails.converters.JSON

class GoodsController {

	static allowedMethods = [allUnits: "POST", editUnit: "POST", allCategories: "POST", editCategory: "POST", 
		deleteCategory: "POST", goodsList: "POST", editGoods: "POST", deleteGoods: "POST",
		queryData: 'POST', queryDataWithSalePrice: 'POST']

	PermissionService permissionService

	UserService userService

	GoodsService goodsService

	def index() {
		if (!permissionService.hasPermission("1001")) {
			redirect controller: "home"
		}

		[loginUserName: userService.loginUserName]
	}

	def goodsUnitIndex() {
		if (!permissionService.hasPermission("1002")) {
			redirect controller: "home"
		}

		[loginUserName: userService.loginUserName]
	}

	def allUnits() {
		def data = goodsService.allUnits()

		render(contentType: "application/json") { array {
			for (u in data) {
				unit id: u.id, name: u.name
			}
		}}
	}

	def editUnit() {
		def result = goodsService.editUnit(params)

		render result as JSON
	}

	def deleteUnit() {
		def result = goodsService.deleteUnit(params.id)

		render result as JSON
	}

	def allCategories() {
		def data = goodsService.allCategories()

		render(contentType: "application/json") { array { 
			for (c in data) {
				category id: c.id, code: c.code, name: c.name
			}
		}}
	}

	def editCategory() {
		def result = goodsService.editCategory(params)

		render result as JSON
	}
	
	def deleteCategory() {
		def result = goodsService.deleteCategory(params.id)
		
		render result as JSON
	}
	
	def goodsList() {
		def data = goodsService.goodsList(params.categoryId)
		
		render(contentType: "application/json") { array {
			for (g in data) {
				goods id: g.id, code: g.code, name: g.name, spec: g.spec, unitId: g.unit.id, unitName: g.unit.name,
					categoryId: g.category.id, salePrice: g.salePrice
			}
		}}
		
		render data as JSON
	}
	
	def editGoods() {
		def result = goodsService.editGoods(params)
		
		render result as JSON	
	}
	
	def deleteGoods() {
		def result = goodsService.deleteGoods(params.id)
		
		render result as JSON
	}
	
	def queryData() {
		def data = goodsService.queryData(params.queryKey)
		
		render(contentType: "application/json") { array {
			for (item in data) {
				goods id: item.id, code: item.code, name: item.name, spec: item.spec,
				unitName: item.unit.name
			}
		}}
	}
	
	def queryDataWithSalePrice() {
		def data = goodsService.queryData(params.queryKey)
		
		render(contentType: "application/json") { array {
			for (item in data) {
				goods id: item.id, code: item.code, name: item.name, spec: item.spec,
				unitName: item.unit.name, salePrice: item.salePrice
			}
		}}
	}
}
