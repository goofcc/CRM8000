package crm8000.Controllers

import crm8000.services.PermissionService;
import crm8000.services.UserService;
import crm8000.services.WarehouseService
import grails.converters.JSON

class WarehouseController {
	static allowedMethods = [warehouseList: 'POST', editWarehouse: 'POST', deleteWarehouse: 'POST',
		queryData: 'POST']
	
	PermissionService permissionService

	UserService userService

	WarehouseService warehouseService

	def index() { 
		if (!permissionService.hasPermission("1003")) {
			redirect controller: "home"
		}

		[loginUserName: userService.loginUserName]
	}
	
	def warehouseList() {
		def data = warehouseService.warehouseList()
		
		render(contentType: "application/json") { array {
			for (w in data) {
				warehouse id: w.id, code: w.code, name: w.name, inited: w.inited
			}
		}}
	}
	
	def editWarehouse() {
		def result = warehouseService.editWarehouse(params)
		
		render result as JSON	
	}
	
	def deleteWarehouse() {
		def result = warehouseService.deleteWarehouse(params.id)
		
		render result as JSON
	}
	
	def queryData() {
		def data = warehouseService.queryData(params.queryKey)
		render(contentType: "application/json") { array {
			for (w in data) {
				warehouse id: w.id, code: w.code, name: w.name
			}
		}}
	}
}