package crm8000.Controllers

import crm8000.services.InvertoryService
import crm8000.services.PermissionService;
import crm8000.services.UserService;

class InvertoryController {

	static allowedMethods = [warehouseList: 'POST', invertoryList: 'POST',
		invertoryDetailList: 'POST']

	PermissionService permissionService

	UserService userService

	InvertoryService invertoryService

	def invQueryIndex() {
		if (!permissionService.hasPermission('2003')) {
			redirect controller: 'home'
		}

		[loginUserName: userService.loginUserName]
	}

	def warehouseList() {
		def data = invertoryService.warehouseList()

		render(contentType: "application/json") {
			array {
				for (w in data) {
					warehouse id: w.id, code: w.code, name: w.name
				}
			}
		}
	}

	def invertoryList() {
		def data = invertoryService.invertoryList(params.warehouseId)
		render(contentType: "application/json") {
			array {
				for (item in data) {
					invertory id: item.id, goodsId: item.goods.id, goodsCode: item.goods.code,
					goodsName: item.goods.name,
					goodsSpec: item.goods.spec, unitName: item.goods.unit.name,
					inCount: item.inCount, inPrice: item.inPrice, inMoney: item.inMoney,
					outCount: item.outCount, outPrice: item.outPrice, outMoney: item.outMoney,
					balanceCount: item.balanceCount, balancePrice: item.balancePrice,
					balanceMoney: item.balanceMoney
				}
			}
		}
	}
	
	def invertoryDetailList() {
		def tc = invertoryService.invertoryDetailListTotalCount(params)
		def data = invertoryService.invertoryDetailList(params)
		render(contentType: "application/json") {
			totalCount = tc
			details = array {
				for (item in data) {
					invertory id: item.id, goodsId: item.goods.id, goodsCode: item.goods.code,
					goodsName: item.goods.name,
					goodsSpec: item.goods.spec, unitName: item.goods.unit.name,
					inCount: item.inCount, inPrice: item.inPrice, inMoney: item.inMoney,
					outCount: item.outCount, outPrice: item.outPrice, outMoney: item.outMoney,
					balanceCount: item.balanceCount, balancePrice: item.balancePrice,
					balanceMoney: item.balanceMoney,
					bizDT: item.bizDate.format("yyyy-MM-dd"),
					bizUserName: item.bizUser.name,
					refType: item.refType,
					refNumber: item.refNumber
				}
			}
		}
	}

}
