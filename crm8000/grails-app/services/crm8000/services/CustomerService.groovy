package crm8000.services

import crm8000.Models.Customer;
import crm8000.Models.CustomerCategory;
import crm8000.Models.CustomerCodeHelp;
import crm8000.helper.PinYinHelper
import crm8000.helper.Result
import grails.transaction.Transactional

@Transactional
class CustomerService {

    def categoryList() {
		return CustomerCategory.listOrderByCode()
    }
	
	Result editCategory(params) {
		if (params.id) {
			// 编辑
			def c = CustomerCategory.findByCodeAndIdNotEqual(params.code, params.id)
			if (c) {
				return new Result(success: false, msg: "编码为${params.code}的客户分类已经存在")
			}
			
			def category = CustomerCategory.get(params.id)
			category.code = params.code
			category.name = params.name
			category.save()
			return new Result(success: true, id: category.id)
		} else {
			// 新增
			def c = CustomerCategory.findByCode(params.code)
			if (c) {
				return new Result(success: false, msg: "编码为${params.code}的客户分类已经存在")
			}
			def category = new CustomerCategory(code: params.code, name: params.name)
			category.save()
			
			return new Result(success: true, id: category.id)
		}
	}
	
	Result deleteCategory(String id) {
		def category = CustomerCategory.get(id)
		def c = Customer.countByCategory(category)
		if (c > 0) {
			return new Result(success: false, msg: "当前客户分类下还有客户资料，不能删除分类")
		}
		
		category.delete()
		
		return new Result(success: true)
	}
	
	def customerList(String categoryId) {
		def category = CustomerCategory.get(categoryId)
		
		return Customer.findAllByCategory(category, [sort: "code"])
	}
	
	Result editCustomer(params) {
		if (params.id) {
			// 编辑
			def c = Customer.countByCodeAndIdNotEqual(params.code, params.id)
			if (c > 0) {
				return new Result(success: false, msg: "编码为${params.code}的客户已经存在")
			}
			
			def customer = Customer.get(params.id)
			customer.code = params.code
			customer.name = params.name
			customer.category = CustomerCategory.get(params.categoryId)
			customer.tel = params.tel
			customer.qq = params.qq
			customer.save()
			
			// 处理拼音字头
			def py = CustomerCodeHelp.findAllByCustomerId(customer.id)
			py*.delete()
			def hcList = new PinYinHelper().toPinYin(customer.name)
			for (hc in hcList) {
				new CustomerCodeHelp(customerId: customer.id, codeHelp: hc.toUpperCase()).save()
			}

			return new Result(success: true, id: customer.id)
			
		} else {
			// 新增
			def c = Customer.countByCode(params.code)
			if (c > 0) {
				return new Result(success: false, msg: "编码为${params.code}的客户已经存在")
			}
			
			def customer = new Customer()
			customer.code = params.code
			customer.name = params.name
			customer.category = CustomerCategory.get(params.categoryId)
			customer.tel = params.tel
			customer.qq = params.qq
			customer.save()
			
			// 处理拼音字头
			def hcList = new PinYinHelper().toPinYin(customer.name)
			for (hc in hcList) {
				new CustomerCodeHelp(customerId: customer.id, codeHelp: hc.toUpperCase()).save()
			}
			
			return new Result(success: true, id: customer.id)
		}
	}
	
	Result deleteCustomer(String id) {
		def customer = Customer.get(id)
		if (!customer) {
			return new Result(success:false, msg: "客户资料不存在")
		}
		
		// TODO 需要判断客户资料是否能删除
		customer.delete()
		
		return new Result(success: true)
	}
	
	def queryData(String queryKey) {
		if (!queryKey) {
			return []
		}
		queryKey += "%";
		def idList = []
		CustomerCodeHelp.findAllByCodeHelpIlike(queryKey).each {
			idList.add(it.customerId)
		}

		return Customer.createCriteria().list  {
			or {
				if (idList.size() > 0) {
					'in' ("id", idList)
				}
				or {
					ilike("code", queryKey)
					ilike("name", queryKey)
				}}
			order "code"
			maxResults 20
		}
	}
}
