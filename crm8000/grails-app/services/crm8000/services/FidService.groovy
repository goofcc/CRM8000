package crm8000.services

import javax.sql.DataSource;

import crm8000.Models.FId
import grails.transaction.Transactional
import groovy.sql.Sql

@Transactional
class FidService {

	DataSource dataSource

	UserService userService

	PermissionService permissionService

	def recentFid() {
		def result = []

		def sql = new Sql(dataSource)

		sql.eachRow ('''
			select f.fid, f.name
            from (select fid as fid, count(fid) as c
            	from t_recent_fid
                where user_id = ?
                group by fid
                order by c desc limit 10 ) v, t_fid f
                where v.fid = f.fid''', [userService.loginUserId])  {
					if (permissionService.hasPermission(it.fid)) {
						result.add(new FId(fid: it.fid, name: it.name))
					}
				}

		return result
	}
}
