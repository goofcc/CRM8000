package crm8000.services

import crm8000.Models.Goods;
import crm8000.Models.GoodsCategory;
import crm8000.Models.GoodsCodeHelp;
import crm8000.Models.GoodsUnit;
import crm8000.helper.PinYinHelper;
import crm8000.helper.Result;
import grails.transaction.Transactional

@Transactional
class GoodsService {

	def allUnits() {
		return GoodsUnit.listOrderByName()
	}

	Result editUnit(params) {
		if (params.id) {
			// 编辑
			def u = GoodsUnit.countByNameAndIdNotEqual(params.name, params.id)
			if (u > 0) {
				return new Result(success: false, msg: "商品计量单位 [${params.name}] 已经存在")
			}

			def unit = GoodsUnit.findById(params.id)
			unit.name = params.name

			unit.save()
			return new Result(success: true, id: unit.id)
		} else {
			// 新增
			def u = GoodsUnit.countByName(params.name)
			if (u > 0) {
				return new Result(success: false, msg: "商品计量单位 [${params.name}] 已经存在")
			}

			def unit = new GoodsUnit(name: params.name)
			unit.save()
			return new Result(success: true, id: unit.id)
		}
	}

	Result deleteUnit(id) {
		def unit = GoodsUnit.findById(id)
		unit?.delete()

		return new Result(success: true)
	}

	def allCategories() {
		return GoodsCategory.listOrderByCode()
	}

	Result editCategory(params) {
		if (params.id) {
			// 编辑
			def c = GoodsCategory.countByCodeAndIdNotEqual(params.code, params.id)
			if (c > 0) {
				return new Result(success: false, msg: "编码为 ${params.code} 的商品分类已经存在")
			}
			def category = GoodsCategory.get(params.id)
			if (category) {
				category.code = params.code
				category.name = params.name
				category.save()

				return new Result(success: true, id: category.id)
			} else {
				return new Result(success: false, msg: "id为${params.id}的商品分类不存在")
			}
		} else {
			// 新增
			def c = GoodsCategory.countByCode(params.code)
			if (c > 0) {
				return new Result(success: false, msg: "编码为 ${params.code} 的商品分类已经存在")
			}

			def category = new GoodsCategory(code: params.code, name: params.name)
			category.save()
			return new Result(success: true, id: category.id)
		}
	}

	Result deleteCategory(id) {
		def category = GoodsCategory.get(id)

		if (!category) {
			return new Result(success: false, msg: "商品分类不存在")
		}

		def c = Goods.countByCategory(category)
		if (c > 0) {
			return new Result(success: false, msg: "当前分类下还有商品，不能删除")
		}

		category.delete()
		return new Result(success: true)
	}

	def goodsList(String categoryId) {
		return Goods.findAllByCategory(GoodsCategory.get(categoryId), [sort: 'code'])
	}

	Result editGoods(params) {
		if (params.salePrice.toBigDecimal() < 0) {
			return new Result(success: false, msg: "销售价格不能是负数")	
		}
		
		if (params.id) {
			// 编辑
			def goods = Goods.get(params.id)
			goods.code = params.code
			goods.name = params.name
			goods.spec = params.spec
			goods.salePrice = params.salePrice.toBigDecimal()

			goods.unit = GoodsUnit.get(params.unitId)
			goods.category = GoodsCategory.get(params.categoryId)
			goods.save()
			
			// 处理拼音字头
			def py = GoodsCodeHelp.findAllByGoodsId(goods.id)
			py*.delete()
			def hcList = new PinYinHelper().toPinYin(goods.name)
			for (hc in hcList) {
				new GoodsCodeHelp(goodsId: goods.id, codeHelp: hc.toUpperCase()).save()
			} 
			
			return new Result(success: true, id: goods.id)
		} else {
			// 新增
			def goods = new Goods()
			goods.code = params.code
			goods.name = params.name
			goods.spec = params.spec
			goods.salePrice = params.salePrice.toBigDecimal()

			goods.unit = GoodsUnit.get(params.unitId)
			goods.category = GoodsCategory.get(params.categoryId)
			goods.save()
			
			// 处理拼音字头
			def hcList = new PinYinHelper().toPinYin(goods.name)
			for (hc in hcList) {
				new GoodsCodeHelp(goodsId: goods.id, codeHelp: hc.toUpperCase()).save()
			}

			return new Result(success: true, id: goods.id)
		}
	}

	Result deleteGoods(String id) {
		// TODO 需要判断商品是否能删除
		Goods.get(id).delete()
		
		// 删除拼音字头
		GoodsCodeHelp.findAllByGoodsId(id)*.delete()
		
		return new Result(success: true)
	}
	
	def queryData(String queryKey) {
		if (!queryKey) {
			return []
		}
		queryKey += "%";
		def idList = []
		GoodsCodeHelp.findAllByCodeHelpIlike(queryKey).each {
			idList.add(it.goodsId)
		}

		return Goods.createCriteria().list  {
			or {
				if (idList.size() > 0) {
					'in' ("id", idList)
				}
				or {
					ilike("code", queryKey)
					ilike("name", queryKey)
				}}
			order "code"
			maxResults 20
		}
	}
}
