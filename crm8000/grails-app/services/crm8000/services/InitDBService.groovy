package crm8000.services

import crm8000.Models.FId;
import crm8000.Models.MenuItem;
import crm8000.Models.Org;
import crm8000.Models.Permission
import crm8000.Models.Role
import crm8000.Models.User;
import grails.transaction.Transactional

@Transactional
class InitDBService {

	def init() {
		initOrg()

		initUser()

		initPermission()

		initFid()

		initMainMenu()
	}

	def private initOrg() {
		if (Org.count() > 0) {
			return
		}

		def org = new Org(name: "公司", fullName: "公司", orgCode: "01", orgLevel: 1)
		org.save()
		new Org(name: "信息部", fullName: "公司\\信息部", parentId: org.id, orgCode: "0199").save()
	}

	def private initUser() {
		def user = User.findByLoginName("admin")

		if (!user) {
			def org = Org.findByFullName("公司\\信息部")

			new User(loginName: "admin", name: "系统管理员", password: "admin".encodeAsMD5(), org: org, orgCode: "019901", enabled: true).save()
		}
	}

	def private initPermission() {
		if (Permission.count() > 0) {
			return
		}

		new Permission(name: "用户管理", fid: "-8999", note: "用户管理").save()
		new Permission(name: "系统日志", fid: "-8997", note: "系统日志").save()
		new Permission(name: "权限管理", fid: "-8996", note: "权限管理").save()
		new Permission(name: "商品", fid: "1001", note: "商品").save()
		new Permission(name: "商品计量单位", fid: "1002", note: "商品计量单位").save()
		new Permission(name: "仓库", fid: "1003", note: "仓库").save()
		new Permission(name: "供应商档案", fid: "1004", note: "供应商档案").save()
		new Permission(name: "客户资料", fid: "1007", note: "客户资料").save()
		new Permission(name: "库存建账", fid: "2000", note: "库存建账").save()
		new Permission(name: "采购入库", fid: "2001", note: "采购入库").save()
		new Permission(name: "销售出库", fid: "2002", note: "销售出库").save()
		new Permission(name: "库存账查询", fid: "2003", note: "库存账查询").save()
		new Permission(name: "应收账款管理", fid: "2004", note: "应收账款管理").save()
		new Permission(name: "应付账款管理", fid: "2005", note: "应付账款管理").save()
		//new Permission(name: "销售退货入库", fid: "2006", note: "销售退货入库").save()
		//new Permission(name: "采购退货出库", fid: "2007", note: "采购退货出库").save()
		
		def roleAdmin = new Role(name: "系统管理员")
		Permission.list().each  { roleAdmin.addToPermission(it) }
		def user = User.findByLoginName("admin")
		roleAdmin.addToUsers(user)
		roleAdmin.save()
	}

	def private initMainMenu() {
		if (MenuItem.count() > 0) {
			return
		}

		// 菜单：文件
		def menuFile = new MenuItem(caption: "文件", showOrder: 1)
		menuFile.save()
		def parentId = menuFile.id
		new MenuItem(parentId: parentId, caption: "首页", showOrder: 1, fid: "-9997").save()
		new MenuItem(parentId: parentId, caption: "重新登录", showOrder: 2, fid: "-9999").save()
		new MenuItem(parentId: parentId, caption: "修改我的密码", showOrder: 3, fid: "-9996").save()
		
		// 菜单：采购
		def menuPurchase = new MenuItem(caption: "采购", showOrder: 2)
		menuPurchase.save()
		parentId = menuPurchase.id
		new MenuItem(parentId: parentId, caption: "采购入库", showOrder: 1, fid: "2001").save()
		//new MenuItem(parentId: parentId, caption: "采购退货出库", showOrder: 2, fid: "2007").save()
		
		// 菜单：库存
		def menuInvertory = new MenuItem(caption: "库存", showOrder: 3)
		menuInvertory.save()
		parentId = menuInvertory.id
		new MenuItem(parentId: parentId, caption: "库存账查询", showOrder: 1, fid: "2003").save()
		new MenuItem(parentId: parentId, caption: "库存建账", showOrder: 3, fid: "2000").save()

		// 菜单：销售
		def menuSale = new MenuItem(caption: "销售", showOrder: 4)
		menuSale.save()
		parentId = menuSale.id
		new MenuItem(parentId: parentId, caption: "销售出库", showOrder: 1, fid: "2002").save()
		//new MenuItem(parentId: parentId, caption: "销售退货入库", showOrder: 2, fid: "2006").save()
		
		// 菜单：客户关系
		def menuCR = new MenuItem(caption: "客户关系", showOrder: 5)
		menuCR.save()
		parentId = menuCR.id
		new MenuItem(parentId: parentId, caption: "客户资料", showOrder: 1, fid: "1007").save()

		// 菜单：资金
		def menuFunds = new MenuItem(caption: "资金", showOrder: 6)
		menuFunds.save()
		parentId = menuFunds.id
		new MenuItem(parentId: parentId, caption: "应收账款管理", showOrder: 1, fid: "2004").save()
		new MenuItem(parentId: parentId, caption: "应付账款管理", showOrder: 2, fid: "2005").save()
		
		// 菜单：基础数据
		def menuBC = new MenuItem(caption: "基础数据", showOrder: 8)
		menuBC.save()
		parentId = menuBC.id
		new MenuItem(parentId: parentId, caption: "商品", showOrder: 1, fid: "1001").save()
		new MenuItem(parentId: parentId, caption: "商品计量单位", showOrder: 2, fid: "1002").save()
		new MenuItem(parentId: parentId, caption: "仓库", showOrder: 3, fid: "1003").save()
		new MenuItem(parentId: parentId, caption: "供应商档案", showOrder: 4, fid: "1004").save()
		
		// 菜单：系统管理
		def menuSystem = new MenuItem(caption: "系统管理", showOrder: 9)
		menuSystem.save()
		parentId = menuSystem.id
		new MenuItem(parentId: parentId, caption: "用户管理", showOrder: 1, fid: "-8999").save()
		new MenuItem(parentId: parentId, caption: "权限管理", showOrder: 2, fid: "-8996").save()
		new MenuItem(parentId: parentId, caption: "系统日志", showOrder: 3, fid: "-8997").save()

	}

	def private initFid() {
		if (FId.count() > 0) {
			return
		}

		new FId(fid: "-9999", name: "重新登录").save()
		new FId(fid: "-9997", name: "首页").save()
		new FId(fid: "-9996", name: "修改我的密码").save()
		new FId(fid: "-8999", name: "用户管理").save()
		new FId(fid: "-8997", name: "系统日志").save()
		new FId(fid: "-8996", name: "权限管理").save()
		new FId(fid: "1001", name: "商品").save()
		new FId(fid: "1002", name: "商品计量单位").save()
		new FId(fid: "1003", name: "仓库").save()
		new FId(fid: "1004", name: "供应商档案").save()
		new FId(fid: "1007", name: "客户资料").save()
		new FId(fid: "2000", name: "库存建账").save()
		new FId(fid: "2001", name: "采购入库").save()
		new FId(fid: "2002", name: "销售出库").save()
		new FId(fid: "2003", name: "库存账查询").save()
		new FId(fid: "2004", name: "应收账款管理").save()
		new FId(fid: "2005", name: "应付账款管理").save()
		//new FId(fid: "2006", name: "销售退货入库").save()
		//new FId(fid: "2007", name: "采购退货出库").save()
	}
}
