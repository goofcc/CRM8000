package crm8000.services

import crm8000.Models.MenuItem
import grails.transaction.Transactional

@Transactional
class MainMenuService {
	PermissionService permissionService

	def getMainMenuItems() {
		def result = new MenuItem()

		def menuLevel1 = MenuItem.findAllByParentId(null, [sort: "showOrder"])

		for (m1 in menuLevel1) {

			def menuLevel2 = MenuItem.findAllByParentId(m1.id, [sort: "showOrder"])

			for (m2 in menuLevel2) {
				def menuLevel3 = MenuItem.findAllByParentId(m2.id, [sort: "showOrder"])
				for (m3 in menuLevel3) {
					if (permissionService.hasPermission(m3.fid)) {
						m2.children.add(m3)
					}
				}
				if (permissionService.hasPermission(m2.fid)) {
					m1.children.add(m2)
				}
			}
			
			if (m1.children.size() > 0) {
				result.children.add(m1)
			}
		}

		return result
	}
}
