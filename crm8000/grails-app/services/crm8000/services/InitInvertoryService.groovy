package crm8000.services

import crm8000.Models.Goods;
import crm8000.Models.GoodsCategory;
import crm8000.Models.Invertory;
import crm8000.Models.InvertoryDetail;
import crm8000.Models.User;
import crm8000.Models.Warehouse;
import crm8000.helper.Result
import crm8000.viewModels.InitInvertoryGoods
import grails.transaction.Transactional

@Transactional
class InitInvertoryService {

	UserService userService

	final String INIT_INVERTORY_CONST = '库存建账'

	def warehouseList() {
		return Warehouse.listOrderByName()
	}

	def initInfoList(String warehouseId) {
		return InvertoryDetail.createCriteria().list {
			warehouse { eq "id", warehouseId }
			eq "refType", INIT_INVERTORY_CONST
			goods { order "code" }
		}
	}

	def goodsCategoryList() {
		return GoodsCategory.list(sort: 'code')
	}

	def goodsList(categoryId, warehouseId) {
		def allGoods = Goods.createCriteria().list  {
			category { eq "id", categoryId }
			order "code"
		}

		def result = []
		for (g in allGoods) {
			def invertGoods = new InitInvertoryGoods(id: g.id, code: g.code, name: g.name,
			spec: g.spec, unit: g.unit.name)

			def initGoods = InvertoryDetail.createCriteria().get {
				warehouse { eq "id", warehouseId }
				goods { eq "id", invertGoods.id }
				eq "refType", INIT_INVERTORY_CONST
			}
			if (initGoods) {
				invertGoods.count = initGoods.balanceCount
				invertGoods.price = initGoods.balancePrice
				invertGoods.money = initGoods.balanceMoney
			}
			result.add(invertGoods)
		}

		return result
	}

	Result commitInitInvertoryGoods(params) {
		String warehouseId = params.warehouseId
		String goodsId = params.goodsId
		
		def warehouseParam = Warehouse.get(warehouseId)
		if (warehouseParam == null) {
			return new Result(success: false, msg: "仓库不存在")
		}
		
		if (warehouseParam.inited) {
			return new Result(success: false, msg: "仓库[${warehouseParam.name}]已经完成库存建账")	
		}
		
		def goodsParam = Goods.get(goodsId)
		if (goodsParam == null) {
			return new Result(success: false, msg: "商品不存在")
		}

		def cnt = InvertoryDetail.createCriteria().count {
			warehouse { eq "id", warehouseId}
			goods {eq "id", goodsId}
			ne "refType", INIT_INVERTORY_CONST
		}

		if (cnt > 0) {
			return new Result(success: false, msg: "已经发生业务，不能再次建账")
		}

		def invertoryDetail = InvertoryDetail.createCriteria().get {
			warehouse { eq "id", warehouseId}
			goods {eq "id", goodsId}
			eq "refType", INIT_INVERTORY_CONST
		}

		Integer goodsCount = params.goodsCount.toInteger()
		BigDecimal goodsMoney = params.goodsMoney.toBigDecimal()
		BigDecimal goodsPrice = goodsCount == 0 ? 0: goodsMoney / goodsCount

		if (invertoryDetail) {
			invertoryDetail.inCount = goodsCount
			invertoryDetail.inMoney = goodsMoney
			invertoryDetail.inPrice = goodsPrice
			invertoryDetail.balanceCount = goodsCount
			invertoryDetail.balanceMoney = goodsMoney
			invertoryDetail.balancePrice = goodsPrice

			invertoryDetail.save()
		} else {
			invertoryDetail = new InvertoryDetail()
			invertoryDetail.warehouse = warehouseParam
			invertoryDetail.goods = goodsParam
			invertoryDetail.bizUser = userService.loginUser
			invertoryDetail.bizDate = new Date()
			invertoryDetail.refType = INIT_INVERTORY_CONST;

			invertoryDetail.inCount = goodsCount
			invertoryDetail.inMoney = goodsMoney
			invertoryDetail.inPrice = goodsPrice
			invertoryDetail.balanceCount = goodsCount
			invertoryDetail.balanceMoney = goodsMoney
			invertoryDetail.balancePrice = goodsPrice

			invertoryDetail.save()
		}

		// 库存总账
		def inv = Invertory.createCriteria().get {
			warehouse { eq "id", warehouseId }
			goods { eq "id", goodsId }
		}
		if (inv) {
			inv.inCount = goodsCount
			inv.inMoney = goodsMoney
			inv.inPrice = goodsPrice
			inv.balanceCount = goodsCount
			inv.balanceMoney = goodsMoney
			inv.balancePrice = goodsPrice
			
			inv.save()
		} else {
			inv = new Invertory()
			inv.warehouse = warehouseParam
			inv.goods = goodsParam
			
			inv.inCount = goodsCount
			inv.inMoney = goodsMoney
			inv.inPrice = goodsPrice
			inv.balanceCount = goodsCount
			inv.balanceMoney = goodsMoney
			inv.balancePrice = goodsPrice
			
			inv.save()
		}

		return new Result(success: true)
	}
	
	Result finish(String warehouseId) {
		def warehouse = Warehouse.get(warehouseId)
		if (!warehouse) {
			return new Result(success: false, msg: "仓库不存在")
		}
		
		if (warehouse.inited) {
			return new Result(success: false, msg: "仓库[${warehouse.name}]已经标记为建账完毕")	
		}
		
		warehouse.inited = true
		warehouse.save()
		
		return new Result(success: true)
	}
	
	Result cancel(String warehouseId) {
		def wh = Warehouse.get(warehouseId)
		if (!wh) {
			return new Result(success: false, msg: "仓库不存在")
		}
		
		if (!wh.inited) {
			return new Result(success: false, msg: "仓库[${wh.name}]没有标记为建账完毕")
		}
		
		def cnt = InvertoryDetail.createCriteria().count {
			warehouse { eq "id", warehouseId }
			ne "refType", INIT_INVERTORY_CONST
		}
		if (cnt > 0) {
			return new Result(success: false, msg: "已经发生业务，不能取消建账完毕标志")
		}
		
		wh.inited = false
		wh.save()
		
		return new Result(success: true)
	}
}
