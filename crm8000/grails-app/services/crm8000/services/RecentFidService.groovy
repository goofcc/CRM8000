package crm8000.services

import crm8000.Models.RecentFid;
import grails.transaction.Transactional

@Transactional
class RecentFidService {

	def UserService userService
	
    def insertRecentFid(String fid) {
		new RecentFid(fid: fid, userId: userService.loginUserId).save()
    }
}
