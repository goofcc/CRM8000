package crm8000.services

import org.codehaus.groovy.grails.web.util.WebUtils;

import crm8000.Models.BizLog;
import crm8000.Models.User;
import grails.transaction.Transactional

@Transactional
class BizLogService {

	def UserService userService
	
	def getRequest(){
		def webUtils = WebUtils.retrieveGrailsWebRequest()
		webUtils.getCurrentRequest()
	}
	
    def log(String info) {
		new BizLog(user: User.get(userService.loginUserId), ip: this.request.getRemoteAddr(), info: info).save()
    }
	
	def logList(params) {
		return BizLog.list(max: params.limit, offset: params.start, sort: 'dateCreated', order: 'desc')
	}
	
	def logListCount() {
		return BizLog.count()
	}
}
