package crm8000.services

import crm8000.Models.Supplier;
import crm8000.Models.SupplierCategory;
import crm8000.Models.SupplierCodeHelp
import crm8000.helper.PinYinHelper
import crm8000.helper.Result
import grails.transaction.Transactional

@Transactional
class SupplierService {

	def categoryList() {
		return SupplierCategory.listOrderByCode()
	}

	Result editCategory(params) {
		if (params.id) {
			// 编辑
			def c = SupplierCategory.countByCodeAndIdNotEqual(params.code, params.id)
			if (c > 0) {
				return new Result(success: false, msg: "编码为${params.code}的供应商分类已经存在")
			}

			def category = SupplierCategory.get(params.id)
			category.code = params.code
			category.name = params.name
			category.save()

			return new Result(success: true, id: category.id)
		} else {
			// 新建
			def c = SupplierCategory.countByCode(params.code)
			if (c > 0) {
				return new Result(success: false, msg: "编码为${params.code}的供应商分类已经存在")
			}

			def category = new SupplierCategory(code: params.code, name: params.code)
			category.save()

			return new Result(success: true, id: category.id)
		}

		return new Result()
	}

	Result deleteCategory(String id) {
		def category = SupplierCategory.get(id)
		if (!category) {
			return new Result(success: false, msg: "供应商分类不存在")
		}

		def c = Supplier.countByCategory(category)
		if (c > 0) {
			return new Result(success: false, msg: "当前分类下还有供应商档案，不能删除该分类")
		}

		category.delete()

		return new Result(success: true)
	}

	def supplierList(String categoryId) {
		return Supplier.findAllByCategory(SupplierCategory.get(categoryId), [sort: 'code'])
	}

	Result editSupplier(params) {
		if (params.id) {
			// 编辑
			def c = Supplier.countByCodeAndIdNotEqual(params.code, params.id)
			if (c > 0) {
				return new Result(success: false, msg: "编码为${params.code}的供应商已经存在")
			}

			def category = SupplierCategory.get(params.categoryId)
			def supplier = Supplier.get(params.id)
			supplier.category = category
			supplier.code = params.code
			supplier.name = params.name
			supplier.tel = params.tel
			supplier.qq = params.qq
			supplier.save()

			// 处理拼音字头
			def py = SupplierCodeHelp.findAllBySupplierId(supplier.id)
			py*.delete()
			def hcList = new PinYinHelper().toPinYin(supplier.name)
			for (hc in hcList) {
				new SupplierCodeHelp(supplierId: supplier.id, codeHelp: hc.toUpperCase()).save()
			}

			return new Result(success: true, id: supplier.id)
		} else {
			// 新建
			def c = Supplier.countByCode(params.code)
			if (c > 0) {
				return new Result(success: false, msg: "编码为${params.code}的供应商已经存在")
			}

			def category = SupplierCategory.get(params.categoryId)
			def supplier = new Supplier()
			supplier.category = category
			supplier.code = params.code
			supplier.name = params.name
			supplier.tel = params.tel
			supplier.qq = params.qq
			supplier.save()

			// 处理拼音字头
			def hcList = new PinYinHelper().toPinYin(supplier.name)
			for (hc in hcList) {
				new SupplierCodeHelp(supplierId: supplier.id, codeHelp: hc.toUpperCase()).save()
			}
			return new Result(success: true, id: supplier.id)
		}
	}

	Result deleteSupplier(String id) {
		def supplier = Supplier.get(id)
		if (!supplier) {
			return new Result(success: false, msg: "供应商档案不存在")
		}

		// TODO 需要判断供应商档案是否能删除
		supplier.delete()

		// 删除拼音字头
		def py = SupplierCodeHelp.findAllBySupplierId(id)
		py*.delete()

		return new Result(success: true)
	}

	def queryData(String queryKey) {
		if (!queryKey) {
			return []
		}
		queryKey += "%";
		def idList = []
		SupplierCodeHelp.findAllByCodeHelpIlike(queryKey).each {
			idList.add(it.supplierId)
		}

		return Supplier.createCriteria().list  {
			or {
				if (idList.size() > 0) {
					'in' ("id", idList)
				}
				or {
					ilike("code", queryKey)
					ilike("name", queryKey)
				}}
			order "code"
			maxResults 20
		}
	}
}