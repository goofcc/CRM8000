hibernate {
	cache.use_second_level_cache = true
	cache.use_query_cache = false
	cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
	singleSession = true // configure OSIV singleSession mode
	flush.mode = 'manual' // OSIV session flush mode outside of transactional context
}

dataSource {
	pooled = true
	jmxExport = true
	driverClassName = "com.mysql.jdbc.Driver"
	username = "root"
	password = "root"
	//logSql = true
}

// environment specific settings
environments {
	development {
		dataSource {
			dbCreate = "update"
			url = "jdbc:mysql://localhost:3306/crm8000?useUnicode=true&characterEncoding=UTF-8"
		}
	}
	production {
		dataSource {
			dbCreate = ""
			url = "jdbc:mysql://localhost:3306/crm8000?useUnicode=true&characterEncoding=UTF-8"
		}
	}
}
