import grails.util.Environment;
import crm8000.services.InitDBService;

class BootStrap {

	def InitDBService initDBService

	def init = { servletContext ->
		if (System.getenv("deploy_oschina") == "1") {
			// 部署到 http://crm8000.oschina.mopaas.com
			initDBService.init()
		} else if (Environment.current == Environment.DEVELOPMENT) {
			initDBService.init()
		}
	}

	def destroy = {
	}
}
