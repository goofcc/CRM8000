package crm8000

class SecurityFilters {

	def filters = {
		loginCheck(controller:'user', invert: true) {
			before = {
				if(!session.loginUserId) {
					redirect(controller: "user", action: "login")
					
					return false
				}
			}
		}
	}
}
