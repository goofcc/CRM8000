﻿Ext.define("CRM8000.Supplier.SupplierEditForm", {
    extend: "Ext.window.Window",

    config: {
        parentForm: null,
        entity: null
    },

    initComponent: function () {
        var me = this;
        var entity = me.getEntity();
        this.adding = entity == null;

        var buttons = [];
        if (!entity) {
            buttons.push({
                text: "保存并继续新增",
                formBind: true,
                handler: function () {
                    me.onOK(true);
                },
                scope: this
            });
        }

        buttons.push({
            text: "保存",
            formBind: true,
            iconCls: "CRM8000-button-ok",
            handler: function () {
                me.onOK(false);
            }, scope: this
        }, {
            text: "取消", handler: function () {
                me.close();
            }, scope: me
        });

        var categoryStore = this.getParentForm().categoryGrid.getStore();

        Ext.apply(me, {
            title: entity == null ? "新增供应商" : "编辑供应商",
            modal: true,
            onEsc: Ext.emptyFn,
            width: 400,
            height: 210,
            layout: "fit",
            defaultFocus: "editCategory",
            items: [
                {
                    id: "editForm",
                    xtype: "form",
                    layout: "form",
                    height: "100%",
                    bodyPadding: 5,
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelWidth: 60,
                        labelAlign: "right",
                        labelSeparator: "",
                        msgTarget: 'side'
                    },
                    items: [
                        {
                            xtype: "hidden",
                            name: "id",
                            value: entity == null ? null : entity.get("id")
                        },
                        {
                            id: "editCategory",
                            xtype: "combo",
                            fieldLabel: "分类",
                            allowBlank: false,
                            blankText: "没有输入供应商分类",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            valueField: "id",
                            displayField: "name",
                            store: categoryStore,
                            queryMode: "local",
                            editable: false,
                            value: categoryStore.getAt(0).get("id"),
                            name: "categoryId",
                            listeners: {
                                specialkey: {
                                    fn: me.onEditCategorySpecialKey,
                                    scope: me
                                }
                            }
                        },
                        {
                            id: "editCode",
                            fieldLabel: "编码",
                            allowBlank: false,
                            blankText: "没有输入供应商编码",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            name: "code",
                            value: entity == null ? null : entity.get("code"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditCodeSpecialKey,
                                    scope: me
                                }
                            }
                        },
                        {
                            id: "editName",
                            fieldLabel: "供应商",
                            allowBlank: false,
                            blankText: "没有输入供应商",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            name: "name",
                            value: entity == null ? null : entity.get("name"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditNameSpecialKey,
                                    scope: me
                                }
                            }
                        },
                        {
                            id: "editTel",
                            fieldLabel: "联系电话",
                            name: "tel",
                            value: entity == null ? null : entity.get("tel"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditTelSpecialKey,
                                    scope: me
                                }
                            }
                        },
                        {
                            id: "editQQ",
                            fieldLabel: "QQ",
                            name: "qq",
                            value: entity == null ? null : entity.get("qq"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditQQSpecialKey,
                                    scope: me
                                }
                            }
                        }
                    ],
                    buttons: buttons
                }],
            listeners: {
                show: {
                    fn: me.onWndShow,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onWndShow: function () {
        if (!this.adding) {
            Ext.getCmp("editCategory").setValue(this.getEntity().get("categoryId"));
        }
    },

    // private
    onOK: function (thenAdd) {
        var me = this;
        var f = Ext.getCmp("editForm");
        var el = f.getEl();
        el.mask(CRM8000.Const.SAVING);
        f.submit({
            url: CRM8000.Const.BASE_URL + "supplier/editSupplier",
            method: "POST",
            success: function (form, action) {
                el.unmask();
                if (thenAdd) {
                    me.getParentForm().freshSupplierGrid(action.result.id);

                    me.clearEdit();
                } else {
                    CRM8000.MsgBox.showInfo("数据保存成功", function () {
                        me.close();
                        me.getParentForm().freshSupplierGrid(action.result.id);
                    });
                }
            },
            failure: function (form, action) {
                el.unmask();
                CRM8000.MsgBox.showInfo(action.result.msg, function () {
                    Ext.getCmp("editCode").focus();
                });
            }
        });
    },

    onEditCategorySpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editCode").focus();
        } else if (e.getKey() == e.BACKSPACE) {
            e.preventDefault();
            return false;
        }
    },

    onEditCodeSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editName").focus();
        }
    },

    onEditNameSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editTel").focus();
        }
    },

    onEditTelSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editQQ").focus();
        }
    },

    onEditQQSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            var f = Ext.getCmp("editForm");
            if (f.getForm().isValid()) {
                var me = this;
                me.onOK(me.adding);
            }
        }
    },

    clearEdit: function () {
        Ext.getCmp("editCategory").focus();

        var editors = [Ext.getCmp("editCode"), Ext.getCmp("editName"), Ext.getCmp("editTel"),
            Ext.getCmp("editQQ")];
        for (var i = 0; i < editors.length; i++) {
            var edit = editors[i];
            edit.setValue(null);
            edit.clearInvalid();
        }
    }
});