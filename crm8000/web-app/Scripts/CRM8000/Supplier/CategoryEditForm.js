﻿Ext.define("CRM8000.Supplier.CategoryEditForm", {
    extend: "Ext.window.Window",

    config: {
        parentForm: null,
        entity: null
    },

    initComponent: function () {
        var me = this;
        var entity = me.getEntity();
        me.adding = entity == null;
        me.__lastId = entity == null ? null : entity.get("id");

        var buttons = [];
        if (!entity) {
            buttons.push({
                text: "保存并继续新增",
                formBind: true,
                handler: function () {
                    me.onOK(true);
                },
                scope: this
            });
        }

        buttons.push({
            text: "保存",
            formBind: true,
            iconCls: "CRM8000-button-ok",
            handler: function () {
                me.onOK(false);
            }, scope: this
        }, {
            text: entity == null ? "关闭" : "取消", handler: function () {
                me.close();
            }, scope: me
        });

        Ext.apply(me, {
            title: entity == null ? "新增供应商分类" : "编辑供应商分类",
            modal: true,
            onEsc: Ext.emptyFn,
            width: 400,
            height: 130,
            layout: "fit",
            defaultFocus: "editCode",
            items: [
                {
                    id: "editForm",
                    xtype: "form",
                    layout: "form",
                    height: "100%",
                    bodyPadding: 5,
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelWidth: 60,
                        labelAlign: "right",
                        labelSeparator: "",
                        msgTarget: 'side'
                    },
                    items: [
                        {
                            xtype: "hidden",
                            name: "id",
                            value: entity == null ? null : entity.get("id")
                        }, {
                            id: "editCode",
                            fieldLabel: "分类编码",
                            allowBlank: false,
                            blankText: "没有输入分类编码",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            name: "code",
                            value: entity == null ? null : entity.get("code"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditCodeSpecialKey,
                                    scope: me
                                }
                            }
                        }, {
                            id: "editName",
                            fieldLabel: "分类名称",
                            allowBlank: false,
                            blankText: "没有输入分类名称",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            name: "name",
                            value: entity == null ? null : entity.get("name"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditNameSpecialKey,
                                    scope: me
                                }
                            }
                        }
                    ]                    
                }
            ],
            buttons: buttons,
            listeners: {
                close: {
                    fn: me.onWndClose,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    // private
    onOK: function (thenAdd) {
        var me = this;
        var f = Ext.getCmp("editForm");
        var el = f.getEl();
        el.mask(CRM8000.Const.SAVING);
        f.submit({
            url: CRM8000.Const.BASE_URL + "supplier/editCategory",
            method: "POST",
            success: function (form, action) {
                el.unmask();
                me.__lastId = action.result.id;
                if (thenAdd) {
                    var editName = Ext.getCmp("editName");
                    editName.setValue(null);
                    editName.clearInvalid();

                    var editCode = Ext.getCmp("editCode");
                    editCode.setValue(null);
                    editCode.clearInvalid();
                } else {
                    CRM8000.MsgBox.showInfo("数据保存成功", function () {
                        me.close();
                    });
                }
            },
            failure: function (form, action) {
                el.unmask();
                CRM8000.MsgBox.showInfo(action.result.msg, function () {
                    Ext.getCmp("editCode").focus();
                });
            }
        });
    },

    onEditCodeSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editName").focus();
        }
    },

    onEditNameSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            var f = Ext.getCmp("editForm");
            if (f.getForm().isValid()) {
                var me = this;
                Ext.getCmp("editCode").focus();
                me.onOK(me.adding);
            }
        }
    },

    onWndClose: function () {
        this.getParentForm().freshCategoryGrid(this.__lastId);
    }
});