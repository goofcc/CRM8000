﻿Ext.define("CRM8000.Supplier.MainForm", {
    extend: "Ext.panel.Panel",

    initComponent: function () {
        var me = this;

        Ext.define("CRM8000SupplierCategory", {
            extend: "Ext.data.Model",
            fields: ["id", "code", "name"]
        });

        var categoryGrid = Ext.create("Ext.grid.Panel", {
            title: "供应商分类",
            forceFit: true,
            columnLines: true,
            columns: [
                { header: "编码", dataIndex: "code", flex: 1, menuDisabled: true, sortable: false },
                { header: "供应商分类", dataIndex: "name", flex: 1, menuDisabled: true, sortable: false }
            ],
            store: Ext.create("Ext.data.Store", {
                model: "CRM8000SupplierCategory",
                autoLoad: false,
                data: []
            }),
            listeners: {
                select: {
                    fn: this.onCategoryGridSelect,
                    scope: this
                },
                itemdblclick: {
                    fn: this.onEditCategory,
                    scope: this
                }
            }
        });
        this.categoryGrid = categoryGrid;

        Ext.define("CRM8000Supplier", {
            extend: "Ext.data.Model",
            fields: ["id", "code", "name", "tel", "qq", "categoryId"]
        });

        var supplierGrid = Ext.create("Ext.grid.Panel", {
            title: "供应商列表",
            columnLines: true,
            columns: [
                { header: "供应商编码", dataIndex: "code", menuDisabled: true, sortable: false },
                { header: "供应商名称", dataIndex: "name", menuDisabled: true, sortable: false, width: 300 },
                { header: "联系电话", dataIndex: "tel", menuDisabled: true, sortable: false },
                { header: "QQ", dataIndex: "qq", menuDisabled: true, sortable: false }
            ],
            store: Ext.create("Ext.data.Store", {
                autoLoad: false,
                model: "CRM8000Supplier",
                data: []
            }),
            listeners: {
                itemdblclick: {
                    fn: this.onEditSupplier,
                    scope: this
                }
            }
        });


        this.supplierGrid = supplierGrid;

        Ext.apply(me, {
            border: 0,
            layout: "border",
            tbar: [
                { text: "新增供应商分类", iconCls: "CRM8000-button-add", handler: this.onAddCategory, scope: this },
                { text: "编辑供应商分类", iconCls: "CRM8000-button-edit", handler: this.onEditCategory, scope: this },
                { text: "删除供应商分类", iconCls: "CRM8000-button-delete", handler: this.onDeleteCategory, scope: this }, "-",
                { text: "新增供应商", iconCls: "CRM8000-button-add-detail", handler: this.onAddSupplier, scope: this },
                { text: "修改供应商", iconCls: "CRM8000-button-edit-detail", handler: this.onEditSupplier, scope: this },
                { text: "删除供应商", iconCls: "CRM8000-button-delete-detail", handler: this.onDeleteSupplier, scope: this }, "-",
                {
                    text: "关闭", iconCls: "CRM8000-button-exit", handler: function () {
                        location.replace(CRM8000.Const.BASE_URL);
                    }
                }
            ],
            items: [
                {
                    region: "center", xtype: "panel", layout: "fit", border: 0,
                    items: [supplierGrid]
                },
                {
                    xtype: "panel",
                    region: "west",
                    layout: "fit",
                    width: 300,
                    minWidth: 200,
                    maxWidth: 350,
                    split: true,
                    border: 0,
                    items: [categoryGrid]
                }
            ]
        });

        me.callParent(arguments);

        this.freshCategoryGrid();
    },

    onAddCategory: function () {
        var form = Ext.create("CRM8000.Supplier.CategoryEditForm", {
            parentForm: this
        });

        form.show();
    },

    onEditCategory: function () {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要编辑的供应商分类");
            return;
        }

        var category = item[0];

        var form = Ext.create("CRM8000.Supplier.CategoryEditForm", {
            parentForm: this,
            entity: category
        });

        form.show();
    },

    onDeleteCategory: function () {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要删除的供应商分类");
            return;
        }

        var category = item[0];
        var info = "请确认是否删除供应商分类: <span style='color:red'>" + category.get("name") + "</span>";
        var me = this;
        CRM8000.MsgBox.confirm(info, function () {
            var el = Ext.getBody();
            el.mask("正在删除中...");
            Ext.Ajax.request({
                url: CRM8000.Const.BASE_URL + "supplier/deleteCategory",
                method: "POST",
                params: { id: category.get("id") },
                callback: function (options, success, response) {
                    el.unmask();

                    if (success) {
                        var data = Ext.JSON.decode(response.responseText);
                        if (data.success) {
                            CRM8000.MsgBox.showInfo("成功完成删除操作", function () {
                                me.freshCategoryGrid();
                            });
                        } else {
                            CRM8000.MsgBox.showInfo(data.msg);
                        }
                    } else {
                        CRM8000.MsgBox.showInfo("网络错误", function () {
                            window.location.reload();
                        });
                    }
                }

            });
        });
    },

    freshCategoryGrid: function (id) {
        var grid = this.categoryGrid;
        var el = grid.getEl() || Ext.getBody();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "supplier/categoryList",
            method: "POST",
            callback: function (options, success, response) {
                var store = grid.getStore();

                store.removeAll();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    store.add(data);

                    if (store.getCount() > 0) {
                        if (id) {
                            var r = store.findExact("id", id);
                            if (r != -1) {
                                grid.getSelectionModel().select(r);
                            }
                        } else {
                            grid.getSelectionModel().select(0);
                        }
                    }
                }

                el.unmask();
            }
        });
    },

    freshSupplierGrid: function (id) {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            return;
        }

        var category = item[0];

        var grid = this.supplierGrid;
        grid.setTitle("属于分类 [" + category.get("name") + "] 的供应商");

        var el = grid.getEl() || Ext.getBody();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "supplier/supplierList",
            params: { id: category.get("id") },
            method: "POST",
            callback: function (options, success, response) {
                var store = grid.getStore();

                store.removeAll();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    store.add(data);

                    grid.setTitle("属于分类 [" + category.get("name") + "] 的供应商" + ", 共计 "
                        + data.length + " 个");

                    if (store.getCount() > 0) {
                        if (id) {
                            var r = store.findExact("id", id);
                            if (r != -1) {
                                grid.getSelectionModel().select(r);
                            }
                        } else {
                            grid.getSelectionModel().select(0);
                        }
                    }
                }

                el.unmask();
            }
        });
    },
    // private
    onCategoryGridSelect: function () {
        this.freshSupplierGrid();
    },

    onAddSupplier: function () {
        if (this.categoryGrid.getStore().getCount() == 0) {
            CRM8000.MsgBox.showInfo("没有供应商分类，请先新增供应商分类");
            return;
        }

        var form = Ext.create("CRM8000.Supplier.SupplierEditForm", {
            parentForm: this
        });

        form.show();
    },

    onEditSupplier: function () {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("没有选择供应商分类");
            return;
        }
        var category = item[0];

        var item = this.supplierGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要编辑的供应商");
            return;
        }

        var supplier = item[0];
        supplier.set("categoryId", category.get("id"));
        var form = Ext.create("CRM8000.Supplier.SupplierEditForm", {
            parentForm: this,
            entity: supplier
        });

        form.show();
    },

    onDeleteSupplier: function () {
        var item = this.supplierGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要删除的供应商");
            return;
        }

        var supplier = item[0];

        var info = "请确认是否删除供应商: <span style='color:red'>" + supplier.get("name") + "</span>";
        var me = this;
        CRM8000.MsgBox.confirm(info, function () {
            var el = Ext.getBody();
            el.mask("正在删除中...");
            Ext.Ajax.request({
                url: CRM8000.Const.BASE_URL + "supplier/deleteSupplier",
                method: "POST",
                params: { id: supplier.get("id") },
                callback: function (options, success, response) {
                    el.unmask();

                    if (success) {
                        var data = Ext.JSON.decode(response.responseText);
                        if (data.success) {
                            CRM8000.MsgBox.showInfo("成功完成删除操作", function () {
                                me.freshSupplierGrid();
                            });
                        } else {
                            CRM8000.MsgBox.showInfo(data.msg);
                        }
                    }
                }

            });
        });
    }
});