﻿Ext.define("CRM8000.Purchase.PRMainForm", {
    extend: "Ext.panel.Panel",

    border: 0,
    layout: "border",

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            tbar: [
                    {
                        text: "关闭", iconCls: "CRM8000-button-exit", handler: function () {
                            location.replace(CRM8000.Const.BASE_URL);
                        }
                    }
            ],
            items: [{
                region: "north", height: "30%",
                split: true, layout: "fit", border: 0,
                items: []
            }, {
                region: "center", layout: "fit", border: 0,
                items: []
            }]
        });

        me.callParent(arguments);
    }
});