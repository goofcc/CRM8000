﻿Ext.define("CRM8000.Purchase.PWDetailEditForm", {
    extend: "Ext.window.Window",

    config: {
        parentForm: null,
        pwBill: null,
        entity: null
    },

    initComponent: function () {
        var me = this;
        var entity = me.getEntity();

        me.adding = entity == null;
        Ext.apply(me, {title: entity == null ? "新建采购入库单商品明细" : "编辑采购入库单商品明细",
            modal: true,
            onEsc: Ext.emptyFn,
            width: 400,
            height: 260,
            layout: "fit",
            defaultFocus: "editGoodsName",
            listeners: {
            	show: {
            		fn: me.onWndShow,
            		scope: me
            	}
            },
            items: [
                {
                    id: "editForm",
                    xtype: "form",
                    layout: "form",
                    height: "100%",
                    bodyPadding: 5,
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelWidth: 60,
                        labelAlign: "right",
                        labelSeparator: "",
                        msgTarget: 'side'
                    },
                    items: [
                        {
                        	xtype: "hidden",
                        	name: "pwBillId",
                        	value: me.getPwBill().get("id")
                        },
                        {
                            xtype: "hidden",
                            name: "id",
                            value: entity == null ? null : entity.get("id")
                        }, {
                        	xtype: "hidden",
                        	name: "goodsId",
                        	id: "editGoodsId"
                        },
                        {
                            id: "editGoodsCode",
                            fieldLabel: "商品编号",
                            xtype: "displayfield"
                        }, {
                        	id: "editGoodsName",
                            fieldLabel: "商品名称",
                            allowBlank: false,
                            blankText: "没有输入商品名称",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            parentCmp: me,
                            xtype: "crm8000_goodsfield",
                            listeners: {
                                specialkey: {
                                    fn: me.onEditGoodsNameSpecialKey,
                                    scope: me
                                }
                            }
                        }, {
                        	id: "editGoodsSpec",
                            fieldLabel: "规格型号",
                            xtype: "displayfield"
                        }, {
                            fieldLabel: "采购数量",
                            allowBlank: false,
                            blankText: "没有输入采购数量",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            xtype: "numberfield",
                            hideTrigger: true,
                            name: "goodsCount",
                            id: "editGoodsCount",
                            listeners: {
                                specialkey: {
                                    fn: me.onEditGoodsCountSpecialKey,
                                    scope: me
                                }
                            }
                        }, {
                        	id: "editGoodsUnit",
                            fieldLabel: "计量单位",
                            xtype: "displayfield"
                        }, {
                            fieldLabel: "采购单价",
                            allowBlank: false,
                            blankText: "没有输入采购单价",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            xtype: "numberfield",
                            hideTrigger: true,
                            name: "goodsPrice",
                            id: "editGoodsPrice",
                            listeners: {
                                specialkey: {
                                    fn: me.onEditGoodsPriceSpecialKey,
                                    scope: me
                                }
                            }
                        }
                    ],
                    buttons: [{
                        text: "保存",
                        iconCls: "CRM8000-button-ok",
                        formBind: true,
                        handler: me.onOK,
                        scope: me
                    }, {
                        text: "取消", handler: function () {
                            me.close();
                        }, scope: me
                    }]
                }
            ]});

        me.callParent(arguments);
    },

    onWndShow: function() {
    	var me = this;

    	if (me.getEntity() == null) {
    		return;
    	}
    	
    	var f = Ext.getCmp("editForm");
        var el = f.getEl();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "purchase/pwBillDetailInfo",
            params: {
            	id: me.getEntity().get("id")
            },
            method: "POST",
            callback: function (options, success, response) {
                el.unmask();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    
                    Ext.getCmp("editGoodsId").setValue(data.goodsId);
                    Ext.getCmp("editGoodsCode").setValue(data.goodsCode);
                    Ext.getCmp("editGoodsName").setValue(data.goodsName);
                    Ext.getCmp("editGoodsSpec").setValue(data.goodsSpec);

                    Ext.getCmp("editGoodsUnit").setValue(data.goodsUnit);
                    Ext.getCmp("editGoodsCount").setValue(data.goodsCount);
                    Ext.getCmp("editGoodsPrice").setValue(data.goodsPrice);
                } else {
                    CRM8000.MsgBox.showInfo("网络错误")
                }
            }
        });
    },

    // private
    onOK: function () {
    	var me = this;
        var f = Ext.getCmp("editForm");
        var el = f.getEl();
        el.mask(CRM8000.Const.SAVING);
        f.submit({
            url: CRM8000.Const.BASE_URL + "purchase/editPWBillDetail",
            method: "POST",
            success: function (form, action) {
                el.unmask();
                if (me.getEntity() == null) {
	                var pf = me.getParentForm();
	                pf.refreshPWBillInfo();
	                pf.refreshPWBillDetailGrid();
	                me.clearEdit();
                } else {
                	me.close();
                	var pf = me.getParentForm();
	                pf.refreshPWBillInfo();
                	pf.refreshPWBillDetailGrid(action.result.id);
                }
            },
            failure: function (form, action) {
                el.unmask();
                CRM8000.MsgBox.showInfo(action.result.msg, function () {
                    Ext.getCmp("editGoodsName").focus();
                });
            }
        });
    },

    onEditCodeSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editName").focus();
        }
    },

    onEditNameSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            var f = Ext.getCmp("editForm");
            if (f.getForm().isValid()) {
                var me = this;
                CRM8000.MsgBox.confirm(this.getConfirmMsg(), function () {
                    me.onOK(me.adding);
                });
            }
        }
    },

    // private
    getConfirmMsg: function () {
        var msg = "请确认是否";
        msg += this.adding ? "新增" : "保存";
        msg += "采购入库单 ?";
        return msg;
    },

    clearEdit: function () {
        Ext.getCmp("editGoodsName").focus();

        var editors = ["editGoodsId", "editGoodsCode", "editGoodsName", "editGoodsSpec", "editGoodsCount", "editGoodsUnit",
                       "editGoodsPrice"];
        for (var i = 0; i < editors.length; i++) {
            var edit = Ext.getCmp(editors[i]);
            if (edit) {
	            edit.setValue(null);
	            edit.clearInvalid();
            }
        }
    },
    
    // GoodsField回调此方法
    __setGoodsInfo: function(data) {
    	Ext.getCmp("editGoodsId").setValue(data.id);
    	Ext.getCmp("editGoodsCode").setValue(data.code);
    	Ext.getCmp("editGoodsSpec").setValue(data.spec);
    	Ext.getCmp("editGoodsUnit").setValue(data.unitName);
    },
    
    onEditGoodsNameSpecialKey: function(field, e) {
    	if (e.getKey() == e.ENTER) {
            Ext.getCmp("editGoodsCount").focus();
        }
    },
    
    onEditGoodsCountSpecialKey: function(field, e) {
    	if (e.getKey() == e.ENTER) {
            Ext.getCmp("editGoodsPrice").focus();
        }
    },
    
    onEditGoodsPriceSpecialKey: function(field, e) {
    	if (e.getKey() == e.ENTER) {
    		var f = Ext.getCmp("editForm");
            if (f.getForm().isValid()) {
            	this.onOK();
            }
        }
    }
});