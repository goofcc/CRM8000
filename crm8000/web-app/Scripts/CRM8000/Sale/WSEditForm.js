﻿Ext.define("CRM8000.Sale.WSEditForm", {
    extend: "Ext.window.Window",

    config: {
        parentForm: null,
        entity: null
    },

    initComponent: function () {
        var me = this;
        var entity = me.getEntity();
        this.adding = entity == null;

        Ext.apply(me, {title: entity == null ? "新建销售出库单主记录" : "编辑销售出库单主记录",
            modal: true,
            onEsc: Ext.emptyFn,
            width: 400,
            height: 230,
            layout: "fit",
            defaultFocus: "editCustomer",
            items: [
                {
                    id: "editForm",
                    xtype: "form",
                    layout: "form",
                    height: "100%",
                    bodyPadding: 5,
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelWidth: 60,
                        labelAlign: "right",
                        labelSeparator: "",
                        msgTarget: 'side'
                    },
                    items: [
                        {
                            xtype: "hidden",
                            id: "hiddenId",
                            name: "id",
                            value: entity == null ? null : entity.get("id")
                        },
                        {
                            id: "editRef",
                            fieldLabel: "单号",
                            xtype: "displayfield",
                            value: "<span style='color:red'>保存后自动生成</span>"
                        },
                        {
                            fieldLabel: "业务日期",
                            allowBlank: false,
                            blankText: "没有输入业务日期",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            xtype: "datefield",
                            format: "Y-m-d",
                            value: new Date(),
                            name: "bizDT",
                            listeners: {
                                specialkey: {
                                    fn: me.onEditBizDTSpecialKey,
                                    scope: me
                                }
                            }
                        },
                        { 
                        	xtype: "hidden",
                        	id: "editCustomerId",
                        	name: "customerId"
                        },
                        {
                            id: "editCustomer",
                            xtype: "crm8000_customerfield",
                            parentCmp: me,
                            fieldLabel: "客户",
                            allowBlank: false,
                            blankText: "没有输入客户",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            listeners: {
                                specialkey: {
                                    fn: me.onEditCustomerSpecialKey,
                                    scope: me
                                }
                            }
                        }, 
                        {
                        	xtype: "hidden",
                        	id: "editWarehouseId",
                        	name: "warehouseId"
                        },
                        {
                        	id: "editWarehouse",
                            fieldLabel: "出库仓库",
                            xtype: "crm8000_warehousefield",
                            parentCmp: me,
                            allowBlank: false,
                            blankText: "没有输入出库仓库",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            listeners: {
                                specialkey: {
                                    fn: me.onEditWarehouseSpecialKey,
                                    scope: me
                                }
                            }
                        }, 
                        {
                        	xtype: "hidden",
                        	id: "editBizUserId",
                        	name: "bizUserId"
                        },
                        {
                        	id: "editBizUser",
                            fieldLabel: "业务员",
                            xtype: "crm8000_userfield",
                            parentCmp: me,
                            allowBlank: false,
                            blankText: "没有输入业务员",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            listeners: {
                                specialkey: {
                                    fn: me.onEditBizUserSpecialKey,
                                    scope: me
                                }
                            }
                        }
                    ],
                    buttons: [{
                        text: "保存",
                        iconCls: "CRM8000-button-ok",
                        formBind: true,
                        handler: me.onOK,
                        scope: me
                    }, {
                        text: "取消", handler: function () {
                            me.close();
                        }, scope: me
                    }]
                }],
                listeners: {
                	show: {
                		fn: me.onWndShow,
                		scope: me
                	}
                }
                });

        me.callParent(arguments);
    },

    onWndShow: function() {
    	var me = this;
    	var f = Ext.getCmp("editForm");
        var el = f.getEl();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "sale/wsBillInfo",
            params: {
            	id: Ext.getCmp("hiddenId").getValue()
            },
            method: "POST",
            callback: function (options, success, response) {
                el.unmask();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    
                    if (data.ref) {
                    	Ext.getCmp("editRef").setValue(data.ref);
                    }
                    
                    Ext.getCmp("editCustomerId").setValue(data.customerId);
                    Ext.getCmp("editCustomer").setValue(data.customerName);
                    
                    Ext.getCmp("editWarehouseId").setValue(data.warehouseId);
                    Ext.getCmp("editWarehouse").setValue(data.warehouseName);

                    Ext.getCmp("editBizUserId").setValue(data.bizUserId);
                    Ext.getCmp("editBizUser").setValue(data.bizUserName);
                } else {
                    CRM8000.MsgBox.showInfo("网络错误")
                }
            }
        });
    },
    
    // private
    onOK: function () {
        var me = this;
        var f = Ext.getCmp("editForm");
        var el = f.getEl();
        el.mask(CRM8000.Const.SAVING);
        f.submit({
            url: CRM8000.Const.BASE_URL + "sale/editWSBill",
            method: "POST",
            success: function (form, action) {
                el.unmask();
                me.close();
                me.getParentForm().refreshWSBillGrid(action.result.id);
            },
            failure: function (form, action) {
                el.unmask();
                CRM8000.MsgBox.showInfo(action.result.msg, function () {
                    Ext.getCmp("editCustomer").focus();
                });
            }
        });
    },

    onEditBizDTSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editCustomer").focus();
        }
    },

    onEditCustomerSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editWarehouse").focus();
        }
    },

    onEditWarehouseSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editBizUser").focus();
        }
    },

    onEditBizUserSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            var f = Ext.getCmp("editForm");
            if (f.getForm().isValid()) {
                var me = this;
                CRM8000.MsgBox.confirm(this.getConfirmMsg(), function () {
                    me.onOK();
                });
            }
        }
    },

    // private
    getConfirmMsg: function () {
        var msg = "请确认是否";
        msg += this.adding ? "新增" : "保存";
        msg += "销售出库单 ?";
        return msg;
    },

    // CustomerField回调此方法
    __setCustomerInfo: function(data) {
    	Ext.getCmp("editCustomerId").setValue(data.id);
    },
    
    // WarehouseField回调此方法
    __setWarehouseInfo: function(data) {
    	Ext.getCmp("editWarehouseId").setValue(data.id);
    },
    
    // UserField回调此方法
    __setUserInfo: function(data) {
    	Ext.getCmp("editBizUserId").setValue(data.id);
    }
});