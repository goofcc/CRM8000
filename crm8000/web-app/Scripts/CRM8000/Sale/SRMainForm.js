﻿//销售退货
Ext.define("CRM8000.Sale.SRMainForm", {
	extend : "Ext.panel.Panel",

	border : 0,
	layout : "border",

	initComponent : function() {
		var me = this;

		Ext.apply(me, {
			tbar : [{
				text : "新建销售退货入库单主记录",
				iconCls : "CRM8000-button-add",
				scope : me,
				handler : me.onAddSRBill
			}, "-", {
				text : "编辑销售退货入库单主记录",
				iconCls : "CRM8000-button-edit",
				scope : me,
				handler : me.onEditSRBill
			}, "-", {
				text : "删除销售退货入库单",
				iconCls : "CRM8000-button-delete",
				scope : me,
				handler : me.onDeleteSRBill
			}, "-", {
				text : "提交入库",
				iconCls : "CRM8000-button-commit",
				scope : me,
				handler : me.onCommit
			}, "-", {
				text : "关闭",
				iconCls : "CRM8000-button-exit",
				handler : function() {
					location.replace(CRM8000.Const.BASE_URL);
				}
			} ],
			items : [ {
				region : "north",
				height : "30%",
				split : true,
				layout : "fit",
				border : 0,
				items : [ me.getSRBillGrid() ]
			}, {
				region : "center",
				layout : "fit",
				border : 0,
				items : [ me.getSRBillDetailGrid() ]
			} ]
		});

		me.callParent(arguments);
    },
    
    getSRBillGrid: function() {
    	var me = this;
    	
    	if (me.__srBillGird) {
    		return me.__srBillGird;
    	}
    	
		Ext.define("CRM8000SRBill", {
			extend : "Ext.data.Model",
			fields : [ "id", "ref", "bizDate", "customerName", "warehouseName",
					"inputUserName", "bizUserName", "billStatus", "amount" ]
		});
		var storeWSBill = Ext.create("Ext.data.Store", {
			autoLoad : false,
			model : "CRM8000SRBill",
			data : []
		});

		me.__srBillGird = Ext.create("Ext.grid.Panel", {
			columnLines : true,
			columns : [ {
				header : "状态",
				dataIndex : "billStatus",
				menuDisabled : true,
				sortable : false,
				width : 60
			}, {
				header : "单号",
				dataIndex : "ref",
				width : 110,
				menuDisabled : true,
				sortable : false
			}, {
				header : "业务日期",
				dataIndex : "bizDate",
				menuDisabled : true,
				sortable : false
			}, {
				header : "客户",
				dataIndex : "customerName",
				width : 200,
				menuDisabled : true,
				sortable : false
			}, {
				header : "退货金额",
				dataIndex : "amount",
				menuDisabled : true,
				sortable : false,
				align : "right",
				xtype : "numbercolumn",
				width : 80
			}, {
				header : "入库仓库",
				dataIndex : "warehouseName",
				menuDisabled : true,
				sortable : false
			}, {
				header : "业务员",
				dataIndex : "bizUserName",
				menuDisabled : true,
				sortable : false
			}, {
				header : "录单人",
				dataIndex : "inputUserName",
				menuDisabled : true,
				sortable : false
			} ],
			listeners : {
				select : {
					fn : me.onWSBillGridSelect,
					scope : me
				},
				itemdblclick : {
					fn : me.onEditWSBill,
					scope : me
				}
			},
			store : storeWSBill
		});
		
		return me.__srBillGird;
    },
    
    getSRBillDetailGrid: function() {
    	var me = this;
    	if (me.__srBillDetailGrid) {
    		return me.__srBillDetailGrid;
    	}
    	
		Ext.define("CRM8000SRBillDetail", {
			extend : "Ext.data.Model",
			fields : [ "id", "goodsCode", "goodsName", "goodsSpec", "unitName",
					"goodsCount", "goodsMoney", "goodsPrice" ]
		});
		var store = Ext.create("Ext.data.Store", {
			autoLoad : false,
			model : "CRM8000SRBillDetail",
			data : []
		});

		me.__srBillDetailGrid = Ext.create("Ext.grid.Panel", {
			title : "销售退货入库单明细",
			columnLines : true,
			columns : [ Ext.create("Ext.grid.RowNumberer", {
				text : "序号",
				width : 30
			}), {
				header : "商品编码",
				dataIndex : "goodsCode",
				menuDisabled : true,
				sortable : false,
				width : 60
			}, {
				header : "商品名称",
				dataIndex : "goodsName",
				menuDisabled : true,
				sortable : false,
				width : 120
			}, {
				header : "规格型号",
				dataIndex : "goodsSpec",
				menuDisabled : true,
				sortable : false
			}, {
				header : "数量",
				dataIndex : "goodsCount",
				menuDisabled : true,
				sortable : false,
				align : "right"
			}, {
				header : "单位",
				dataIndex : "unitName",
				menuDisabled : true,
				sortable : false,
				width : 60
			}, {
				header : "原销售单价",
				dataIndex : "goodsPrice",
				menuDisabled : true,
				sortable : false,
				align : "right",
				xtype : "numbercolumn",
				width : 80
			}, {
				header : "退货金额",
				dataIndex : "goodsMoney",
				menuDisabled : true,
				sortable : false,
				align : "right",
				xtype : "numbercolumn",
				width : 80
			} ],
			tbar : [ {
				text : "新建销售退货入库单商品明细",
				iconCls : "CRM8000-button-add-detail",
				scope : me,
				handler : me.onAddSRBillDetail
			}, "-", {
				text : "编辑销售退货入库单商品明细",
				iconCls : "CRM8000-button-edit-detail",
				scope : me,
				handler : me.onEditSRBillDetail
			}, "-", {
				text : "删除销售退货入库单商品明细",
				iconCls : "CRM8000-button-delete-detail",
				scope : me,
				handler : me.onDeleteSRBillDetail
			} ],
			listeners : {
				itemdblclick : {
					fn : me.onEditSRBillDetail,
					scope : me
				}
			},
			store : store
		});

		return me.__srBillDetailGrid;
    },
    
    onAddSRBill: function() {
    	
    }
});