﻿Ext.define("CRM8000.App", {
    config: {
        userName: ""
    },

    constructor: function (config) {
        this.initConfig(config);

        this.__createMainUI();
    },

    __createMainUI: function () {
        var me = this;

        me.mainPanel = Ext.create("Ext.panel.Panel", {
            border: 0,
            layout: "fit"
        });

        if (!Ext.ModelManager.isRegistered("CRM8000FId")) {
            Ext.define("CRM8000FId", {
                extend: "Ext.data.Model",
                fields: ["id", "name"]
            });
        }

        var storeRecentFid = Ext.create("Ext.data.Store", {
            autoLoad: false,
            model: "CRM8000FId",
            data: []
        });

        me.gridRecentFid = Ext.create("Ext.grid.Panel", {
            forceFit: true,
            columns: [
                {
                    dataIndex: "name", menuDisabled: true,
                    renderer: function (value, metaData, record) {
                        var fid = record.get("id");
                        var fileName = CRM8000.Const.BASE_URL + "Images/fid/fid" + fid + ".png";
                        return "<img src='"  + fileName  + "'><a href='#' style='text-decoration:none'>" + value + "</a></img>";
                    }
                }
            ],
            store: storeRecentFid
        });

        me.gridRecentFid.on("itemclick", function (v, r) {
            var fid = r.get("id");

            if (fid == "-9999") {
                CRM8000.MsgBox.confirm("请确认是否重新登录", function () {
                    location.replace(CRM8000.Const.BASE_URL + "home/navigateTo?fid=-9999");
                });
            } else {
                location.replace(CRM8000.Const.BASE_URL + "home/navigateTo?fid=" + fid);
            }
        }, this);

        me.vp = Ext.create("Ext.container.Viewport", {
            layout: "fit",
            items: [{
                id: "__CRM8000TopPanel",
                xtype: "panel",
                border: 0,
                layout: "border",
                bbar: ["当前用户：<span style='color:red'>" + me.getUserName() + "</span>"],
                items: [{
                    region: "center", layout: "fit", xtype: "panel",
                    items: [me.mainPanel]
                },{
                    xtype: "panel",
                    region: "east",
                    width: 250,
                    minWidth: 200,
                    maxWidth: 350,
                    split: true,
                    collapsible: true,
                    collapsed: me.getRecentFidPanelCollapsed(),
                    header: false,
                    layout: { type: "vbox", align: "stretch" },
                    items: [
                        {
                            xtype: "panel", title: "常用功能", flex: 1, border: 0,
                            layout: "fit",
                            items: [me.gridRecentFid]
                        }
                    ],
                    listeners: {
                    	collapse:{
                    		fn: me.onRecentFidPanelCollapse,
                    		scope: me
                    	},
                    	expand: {
                    		fn: me.onRecentFidPanelExpand,
                    		scope: me
                    	}
                    }
                }
                ]
            }]
        });

        var el = Ext.getBody();
        el.mask("系统正在加载中...");

        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "home/mainMenuItems",
            method: "POST",
            callback: function (opt, success, response) {
                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    me.createMainMenu(data);
                    me.refreshRectFidGrid();
                }
                
                el.unmask();
            },
            scope: me
        });
    },

    // private
    refreshRectFidGrid: function () {
        var me = this;

        var el = me.gridRecentFid.getEl() || Ext.getBody();
        el.mask("系统正在加载中...");
        var store = me.gridRecentFid.getStore();
        store.removeAll();

        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "home/recentFid",
            method: "POST",
            callback: function (opt, success, response) {
                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    store.add(data.root);
                }
                el.unmask();
            },
            scope: me
        });
    },

    // private
    createMainMenu: function (root) {
    	var me = this;

        var menuItemClick = function () {
            if (this.fid == "-9999") {
                CRM8000.MsgBox.confirm("请确认是否重新登录", function () {
                    location.replace(CRM8000.Const.BASE_URL + "home/navigateTo?fid=-9999");
                });
            } else {
                location.replace(CRM8000.Const.BASE_URL + "home/navigateTo?fid=" + this.fid);
            }
        };

        var mainMenu = [];
        for (var i = 0; i < root.length; i++) {
            var m1 = root[i];

            var menuItem = Ext.create("Ext.menu.Menu");
            for (var j = 0; j < m1.children.length; j++) {
                var m2 = m1.children[j];

                if (m2.children.length == 0) {
                    // 只有二级菜单
                    menuItem.add({
                        text: m2.caption, fid: m2.fid, handler: menuItemClick,
                        iconCls: "CRM8000-fid" + m2.fid
                    });
                } else {
                    var menuItem2 = Ext.create("Ext.menu.Menu");

                    menuItem.add({ text: m2.caption, menu: menuItem2 });

                    // 三级菜单
                    for (var k = 0; k < m2.children.length; k++) {
                        var m3 = m2.children[k];
                        menuItem2.add({
                            text: m3.caption, fid: m3.fid, handler: menuItemClick,
                            iconCls: "CRM8000-fid" + m3.fid
                        });
                    }
                }
            }

            mainMenu.push({ text: m1.caption, menu: menuItem });
        }

        var mainToolbar = Ext.create("Ext.toolbar.Toolbar", {
            dock: "top"
        });
        mainToolbar.add(mainMenu);

        this.vp.getComponent(0).addDocked(mainToolbar);
    },

    setAppHeader: function (header) {
        if (!header) {
            return;
        }
        var panel = Ext.getCmp("__CRM8000TopPanel");
        panel.setTitle(header.title + " - CRM8000");
        panel.setIconCls(header.iconCls);
    },

    add: function (comp) {
        this.mainPanel.add(comp);
    },
    
    onRecentFidPanelCollapse: function() {
    	Ext.util.Cookies.set("CRM8000_RECENT_FID", "1", Ext.Date.add(new Date(), Ext.Date.YEAR, 1));
    },
    
    onRecentFidPanelExpand: function() {
    	Ext.util.Cookies.clear("CRM8000_RECENT_FID");
    },
    
    getRecentFidPanelCollapsed: function() {
    	var v = Ext.util.Cookies.get("CRM8000_RECENT_FID");
    	return v == "1";
    }
});