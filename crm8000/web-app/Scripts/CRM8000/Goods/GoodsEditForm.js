﻿Ext.define("CRM8000.Goods.GoodsEditForm", {
    extend: "Ext.window.Window",

    config: {
        parentForm: null,
        entity: null
    },

    initComponent: function () {
        var me = this;
        var entity = me.getEntity();

        Ext.define("CRM8000GoodsUnit", {
            extend: "Ext.data.Model",
            fields: ["id", "name"]
        });

        var unitStore = Ext.create("Ext.data.Store", {
            model: "CRM8000GoodsUnit",
            autoLoad: false,
            data: []
        });
        me.unitStore = unitStore;

        me.adding = entity == null;

        var buttons = [];
        if (!entity) {
            buttons.push({
                text: "保存并继续新增",
                formBind: true,
                handler: function () {
                    me.onOK(true);
                },
                scope: me
            });
        }

        buttons.push({
            text: "保存",
            formBind: true,
            iconCls: "CRM8000-button-ok",
            handler: function () {
                me.onOK(false);
            }, scope: me
        }, {
            text: entity == null ? "关闭" : "取消", handler: function () {
                me.close();
            }, scope: me
        });

        var categoryStore = me.getParentForm().categoryGrid.getStore();
        var selectedCategory = me.getParentForm().categoryGrid.getSelectionModel().getSelection();
        var defaultCategoryId = null;
        if (selectedCategory != null && selectedCategory.length > 0) {
            defaultCategoryId = selectedCategory[0].get("id");
        }

        Ext.apply(me, {
            title: entity == null ? "新增商品" : "编辑商品",
            modal: true,
            onEsc: Ext.emptyFn,
            width: 400,
            height: 230,
            layout: "fit",
            defaultFocus: "editCode",
            items: [
                {
                    id: "editForm",
                    xtype: "form",
                    layout: "form",
                    height: "100%",
                    bodyPadding: 5,
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelWidth: 60,
                        labelAlign: "right",
                        labelSeparator: "",
                        msgTarget: 'side'
                    },
                    items: [
                        {
                            xtype: "hidden",
                            name: "id",
                            value: entity == null ? null : entity.get("id")
                        },
                        {
                            id: "editCategory",
                            xtype: "combo",
                            fieldLabel: "商品分类",
                            allowBlank: false,
                            blankText: "没有输入商品分类",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            valueField: "id",
                            displayField: "name",
                            store: categoryStore,
                            queryMode: "local",
                            editable: false,
                            value: defaultCategoryId,
                            name: "categoryId",
                            listeners: {
                                specialkey: {
                                    fn: me.onEditCategorySpecialKey,
                                    scope: me
                                }
                            }
                        },
                        {
                            id: "editCode",
                            fieldLabel: "商品编码",
                            allowBlank: false,
                            blankText: "没有输入商品编码",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            name: "code",
                            value: entity == null ? null : entity.get("code"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditCodeSpecialKey,
                                    scope: me
                                }
                            }
                        },
                        {
                            id: "editName",
                            fieldLabel: "品名",
                            allowBlank: false,
                            blankText: "没有输入品名",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            name: "name",
                            value: entity == null ? null : entity.get("name"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditNameSpecialKey,
                                    scope: me
                                }
                            }
                        },
                        {
                            id: "editSpec",
                            fieldLabel: "规格型号",
                            name: "spec",
                            value: entity == null ? null : entity.get("spec"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditSpecSpecialKey,
                                    scope: me
                                }
                            }
                        },
                        {
                            id: "editUnit",
                            xtype: "combo",
                            fieldLabel: "计量单位",
                            allowBlank: false,
                            blankText: "没有输入计量单位",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            valueField: "id",
                            displayField: "name",
                            store: unitStore,
                            queryMode: "local",
                            editable: false,
                            name: "unitId",
                            listeners: {
                                specialkey: {
                                    fn: me.onEditUnitSpecialKey,
                                    scope: me
                                }
                            }
                        }, {
                            fieldLabel: "销售价",
                            allowBlank: false,
                            blankText: "没有输入销售价",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            xtype: "numberfield",
                            hideTrigger: true,
                            name: "salePrice",
                            id: "editSalePrice",
                            value: entity == null ? null : entity.get("salePrice"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditSalePriceSpecialKey,
                                    scope: me
                                }
                            }
                        }
                    ],
                    buttons: buttons
                }],
            listeners: {
                show: {
                    fn: me.onWndShow,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onWndShow: function () {
        var me = this;
        var el = me.getEl();
        var unitStore = me.unitStore;
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "goods/allUnits",
            method: "POST",
            callback: function (options, success, response) {
                unitStore.removeAll();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    unitStore.add(data);
                }

                el.unmask();

                if (!me.adding) {
                    Ext.getCmp("editCategory").setValue(me.getEntity().get("categoryId"));
                    Ext.getCmp("editUnit").setValue(me.getEntity().get("unitId"));
                } else {
                    if (unitStore.getCount() > 0) {
                        var unitId = unitStore.getAt(0).get("id");
                        Ext.getCmp("editUnit").setValue(unitId);
                    }
                }
            }
        });
    },

    // private
    onOK: function (thenAdd) {
        var me = this;
        var f = Ext.getCmp("editForm");
        var el = f.getEl();
        el.mask(CRM8000.Const.SAVING);
        f.submit({
            url: CRM8000.Const.BASE_URL + "goods/editGoods",
            method: "POST",
            success: function (form, action) {
                el.unmask();
                if (thenAdd) {
                    me.getParentForm().freshGoodsGrid();

                    me.clearEdit();
                } else {
                    CRM8000.MsgBox.showInfo("数据保存成功", function () {
                        me.close();
                        me.getParentForm().freshGoodsGrid(action.result.id);
                    });
                }
            },
            failure: function (form, action) {
                el.unmask();
                CRM8000.MsgBox.showInfo(action.result.msg, function () {
                    Ext.getCmp("editCode").focus();
                });
            }
        });
    },

    onEditCategorySpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editCode").focus();
        }
    },

    onEditCodeSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editName").focus();
        }
    },

    onEditNameSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editSpec").focus();
        }
    },

    onEditSpecSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editUnit").focus();
        }
    },

    onEditUnitSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
        	Ext.getCmp("editSalePrice").focus();
        }
    },

    onEditSalePriceSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            var f = Ext.getCmp("editForm");
            if (f.getForm().isValid()) {
                var me = this;
                me.onOK(me.adding);
            }
        }
    },

    clearEdit: function () {
        Ext.getCmp("editCode").focus();

        var editors = [Ext.getCmp("editCode"), Ext.getCmp("editName"), Ext.getCmp("editSpec"),
                       Ext.getCmp("editSalePrice")];
        for (var i = 0; i < editors.length; i++) {
            var edit = editors[i];
            edit.setValue(null);
            edit.clearInvalid();
        }
    }
});