﻿Ext.define("CRM8000.Goods.UnitMainForm", {
    extend: "Ext.panel.Panel",

    initComponent: function () {
        var me = this;

        Ext.define("CRM8000GoodsUnit", {
            extend: "Ext.data.Model",
            fields: ["id", "name"]
        });

        var grid = Ext.create("Ext.grid.Panel", {
        	columnLines: true,
            columns: [
                { header: "计量单位", dataIndex: "name", menuDisabled: true, sortable: false, width: 200 }
            ],
            store: Ext.create("Ext.data.Store", {
                model: "CRM8000GoodsUnit",
                autoLoad: false,
                data: []
            }),
            listeners: {
                itemdblclick: {
                    fn: me.onEditUnit,
                    scope: me
                }
            }
        });
        this.grid = grid;

        Ext.apply(me, {
            border: 0,
            layout: "border",
            tbar: [
                { text: "新增计量单位", iconCls: "CRM8000-button-add", handler: me.onAddUnit, scope: me },
                { text: "编辑计量单位", iconCls: "CRM8000-button-edit", handler: me.onEditUnit, scope: me },
                { text: "删除计量单位", iconCls: "CRM8000-button-delete", handler: me.onDeleteUnit, scope: me }, "-",
                {
                    text: "关闭", iconCls: "CRM8000-button-exit", handler: function () {
                        location.replace(CRM8000.Const.BASE_URL);
                    }
                }
            ],
            items: [
                {
                    region: "center", xtype: "panel", layout: "fit", border: 0,
                    items: [grid]
                }
            ]
        });

        me.callParent(arguments);

        me.freshGrid();
    },

    onAddUnit: function () {
        var form = Ext.create("CRM8000.Goods.UnitEditForm", {
            parentForm: this
        });

        form.show();
    },

    onEditUnit: function () {
        var item = this.grid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要编辑的商品计量单位");
            return;
        }

        var unit = item[0];

        var form = Ext.create("CRM8000.Goods.UnitEditForm", {
            parentForm: this,
            entity: unit
        });

        form.show();
    },

    onDeleteUnit: function () {
        var item = this.grid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要删除的商品计量单位");
            return;
        }

        var me = this;
        var unit = item[0];
        var info = "请确认是否删除商品计量单位 <span style='color:red'>" + unit.get("name") + "</span> ?";

        CRM8000.MsgBox.confirm(info, function () {
            var el = Ext.getBody();
            el.mask(CRM8000.Const.LOADING);
            Ext.Ajax.request({
                url: CRM8000.Const.BASE_URL + "goods/deleteUnit",
                params: { id: unit.get("id") },
                method: "POST",
                callback: function (options, success, response) {
                    el.unmask();
                    if (success) {
                        var data = Ext.JSON.decode(response.responseText);
                        if (data.success) {
                            CRM8000.MsgBox.showInfo("成功完成删除操作", function () {
                                me.freshGrid();
                            });
                        } else {
                            CRM8000.MsgBox.showInfo(data.msg);
                        }
                    } else {
                        CRM8000.MsgBox.showInfo("网络错误");
                    }
                }
            });
        });
    },

    freshGrid: function() {
    	var me = this;
        var grid = me.grid;
        
        var el = grid.getEl() || Ext.getBody();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "goods/allUnits",
            method: "POST",
            callback: function (options, success, response) {
                var store = grid.getStore();

                store.removeAll();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    store.add(data);
                }

                el.unmask();	
            }
        });
    }
});