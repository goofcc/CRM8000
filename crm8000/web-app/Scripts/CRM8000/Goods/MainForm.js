﻿Ext.define("CRM8000.Goods.MainForm", {
    extend: "Ext.panel.Panel",

    initComponent: function () {
        var me = this;

        Ext.define("CRM8000GoodsCategory", {
            extend: "Ext.data.Model",
            fields: ["id", "code", "name"]
        });

        var categoryGrid = Ext.create("Ext.grid.Panel", {
            title: "商品分类",
            forceFit: true,
            columnLines: true,
            columns: [
                { header: "编码", dataIndex: "code", flex: 1, menuDisabled: true, sortable: false },
                { header: "类别", dataIndex: "name", flex: 1, menuDisabled: true, sortable: false }
            ],
            store: Ext.create("Ext.data.Store", {
                model: "CRM8000GoodsCategory",
                autoLoad: false,
                data: []
            }),
            listeners: {
                select: {
                    fn: me.onCategoryGridSelect,
                    scope: me
                },
                itemdblclick: {
                    fn: me.onEditCategory,
                    scope: me
                }
            }
        });
        me.categoryGrid = categoryGrid;

        Ext.define("CRM8000Goods", {
            extend: "Ext.data.Model",
            fields: ["goodsId", "code", "name", "spec", "unitId", "unitName", "categoryId", "salePrice"]
        });

        var goodsGrid = Ext.create("Ext.grid.Panel", {
            title: "商品列表",
            columnLines: true,
            columns: [
                { header: "商品编码", dataIndex: "code", menuDisabled: true, sortable: false },
                { header: "品名", dataIndex: "name", menuDisabled: true, sortable: false, width: 300 },
                { header: "规格型号", dataIndex: "spec", menuDisabled: true, sortable: false, widht: 200 },
                { header: "计量单位", dataIndex: "unitName", menuDisabled: true, sortable: false },
                { header: "销售价", dataIndex: "salePrice", menuDisabled: true, sortable: false, align: "right", xtype: "numbercolumn" }
            ],
            store: Ext.create("Ext.data.Store", {
                autoLoad: false,
                model: "CRM8000Goods",
                data: []
            }),
            listeners: {
                itemdblclick: {
                    fn: me.onEditGoods,
                    scope: me
                }
            }
        });

        me.goodsGrid = goodsGrid;

        Ext.apply(me, {
            border: 0,
            layout: "border",
            tbar: [
                { text: "新增商品分类", iconCls: "CRM8000-button-add", handler: me.onAddCategory, scope: me },
                { text: "编辑商品分类", iconCls: "CRM8000-button-edit", handler: me.onEditCategory, scope: me },
                { text: "删除商品分类", iconCls: "CRM8000-button-delete", handler: me.onDeleteCategory, scope: me }, "-",
                { text: "新增商品", iconCls: "CRM8000-button-add-detail", handler: me.onAddGoods, scope: me },
                { text: "修改商品", iconCls: "CRM8000-button-edit-detail", handler: me.onEditGoods, scope: me },
                { text: "删除商品", iconCls: "CRM8000-button-delete-detail", handler: me.onDeleteGoods, scope: me }, "-",
                {
                    text: "关闭", iconCls: "CRM8000-button-exit", handler: function () {
                        location.replace(CRM8000.Const.BASE_URL);
                    }
                }
            ],
            items: [
                {
                    region: "center", xtype: "panel", layout: "fit", border: 0,
                    items: [goodsGrid]
                },
                {
                    xtype: "panel",
                    region: "west",
                    layout: "fit",
                    width: 300,
                    minWidth: 200,
                    maxWidth: 350,
                    split: true,
                    border: 0,
                    items: [categoryGrid]
                }
            ]
        });

        me.callParent(arguments);

        me.freshCategoryGrid();
    },

    onAddCategory: function () {
        var form = Ext.create("CRM8000.Goods.CategoryEditForm", {
            parentForm: this
        });

        form.show();
    },

    onEditCategory: function () {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要编辑的商品分类");
            return;
        }

        var category = item[0];

        var form = Ext.create("CRM8000.Goods.CategoryEditForm", {
            parentForm: this,
            entity: category
        });

        form.show();
    },

    onDeleteCategory: function () {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要删除的商品分类");
            return;
        }

        var category = item[0];
        var info = "请确认是否删除商品分类: <span style='color:red'>" + category.get("name") + "</span>";
        var me = this;
        CRM8000.MsgBox.confirm(info, function () {
            var el = Ext.getBody();
            el.mask("正在删除中...");
            Ext.Ajax.request({
                url: CRM8000.Const.BASE_URL + "goods/deleteCategory",
                method: "POST",
                params: { id: category.get("id") },
                callback: function (options, success, response) {
                    el.unmask();

                    if (success) {
                        var data = Ext.JSON.decode(response.responseText);
                        if (data.success) {
                            CRM8000.MsgBox.showInfo("成功完成删除操作", function () {
                                me.freshCategoryGrid();
                            });
                        } else {
                            CRM8000.MsgBox.showInfo(data.msg);
                        }
                    } else {
                        CRM8000.MsgBox.showInfo("网络错误", function () {
                            window.location.reload();
                        });
                    }
                }

            });
        });
    },

    freshCategoryGrid: function (id) {
        var grid = this.categoryGrid;
        var el = grid.getEl() || Ext.getBody();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "goods/allCategories",
            method: "POST",
            callback: function (options, success, response) {
                var store = grid.getStore();

                store.removeAll();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    store.add(data);

                    if (store.getCount() > 0) {
                        if (id) {
                            var r = store.findExact("id", id);
                            if (r != -1) {
                                grid.getSelectionModel().select(r);
                            }
                        } else {
                            grid.getSelectionModel().select(0);
                        }
                    }
                }

                el.unmask();
            }
        });
    },

    freshGoodsGrid: function (id) {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            return;
        }

        var category = item[0];

        var grid = this.goodsGrid;
        grid.setTitle("属于分类 [" + category.get("name") + "] 的商品");

        var el = grid.getEl();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "goods/goodsList",
            params: { categoryId: category.get("id") },
            method: "POST",
            callback: function (options, success, response) {
                var store = grid.getStore();

                store.removeAll();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);

                    grid.setTitle("属于分类 [" + category.get("name") + "] 的商品" + ", 共计 "
                        + data.length + " 种");
                    store.add(data);

                    if (id) {
                        var r = store.findExact("id", id);
                        if (r != -1) {
                            grid.getSelectionModel().select(r);
                        }
                    }
                }

                el.unmask();
            }
        });
    },
    // private
    onCategoryGridSelect: function () {
        this.freshGoodsGrid();
    },

    onAddGoods: function () {
        if (this.categoryGrid.getStore().getCount() == 0) {
            CRM8000.MsgBox.showInfo("没有商品分类，请先新增商品分类");
            return;
        }

        var form = Ext.create("CRM8000.Goods.GoodsEditForm", {
            parentForm: this
        });

        form.show();
    },

    onEditGoods: function () {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择商品分类");
            return;
        }

        var category = item[0];

        var item = this.goodsGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要编辑的商品");
            return;
        }

        var goods = item[0];
        goods.set("categoryId", category.get("id"));
        var form = Ext.create("CRM8000.Goods.GoodsEditForm", {
            parentForm: this,
            entity: goods
        });

        form.show();
    },

    onDeleteGoods: function () {
        var item = this.goodsGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要删除的商品");
            return;
        }

        var goods = item[0];

        var info = "请确认是否删除商品: <span style='color:red'>" + goods.get("name") + "</span>";
        var me = this;
        CRM8000.MsgBox.confirm(info, function () {
            var el = Ext.getBody();
            el.mask("正在删除中...");
            Ext.Ajax.request({
                url: CRM8000.Const.BASE_URL + "goods/deleteGoods",
                method: "POST",
                params: { id: goods.get("id") },
                callback: function (options, success, response) {
                    el.unmask();

                    if (success) {
                        var data = Ext.JSON.decode(response.responseText);
                        if (data.success) {
                            CRM8000.MsgBox.showInfo("成功完成删除操作", function () {
                                me.freshGoodsGrid();
                            });
                        } else {
                            CRM8000.MsgBox.showInfo(data.msg);
                        }
                    } else {
                        CRM8000.MsgBox.showInfo("网络错误", function () {
                            window.location.reload();
                        });
                    }
                }

            });
        });
    }
});