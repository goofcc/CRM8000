﻿Ext.define("CRM8000.Log.MainForm", {
    extend: "Ext.panel.Panel",

    initComponent: function () {
        var me = this;
        Ext.define("CRM8000Log", {
            extend: "Ext.data.Model",
            fields: ["id", "loginName", "userName", "ip", "content", "dt"],
            idProperty: "id"
        });
        var store = Ext.create("Ext.data.Store", {
            model: "CRM8000Log",
            pageSize: 20,
            proxy: {
                type: "ajax",
                extraParams: {
                },
                actionMethods: {
                	read: "POST"
                },
                url: CRM8000.Const.BASE_URL + "log/logList",
                reader: {
                    root: 'logs',
                    totalProperty: 'totalCount'
                }
            },
            autoLoad: true
        });

        var grid = Ext.create("Ext.grid.Panel", {
            loadMask: true,
            border: 0,
            columnLines: true,
            columns: [
                Ext.create("Ext.grid.RowNumberer", { text: "序号", width: 30 }),
                { text: "登录名", dataIndex: "loginName", menuDisabled: true },
                { text: "姓名", dataIndex: "userName", menuDisabled: true },
                { text: "IP", dataIndex: "ip", menuDisabled: true },
                { text: "日志内容", dataIndex: "content", flex: 1, menuDisabled: true },
                { text: "日志记录时间", dataIndex: "dt", width: 150, menuDisabled: true }
            ],
            store: store,
            tbar: {
            	xtype: "pagingtoolbar",
            	store: store
            }, 
            bbar: {
            	xtype: "pagingtoolbar",
            	store: store
            }
        });

        me.__grid = grid;

        Ext.apply(me, {
            border: 0,
            layout: "border",
            tbar: [
                { text: "刷新", handler: me.onRefresh, scope: me, iconCls: "CRM8000-button-refresh" },
                "-",
                {
                    text: "关闭", iconCls: "CRM8000-button-exit", handler: function () {
                        location.replace(CRM8000.Const.BASE_URL);
                    }
                }
            ],
            items: [
                {
                    region: "center", layout: "fit", xtype: "panel", border: 0,
                    items: [grid]
                }
            ]
        });

        me.callParent(arguments);
    },

    // private
    onRefresh: function () {
        this.__grid.getStore().reload();
    }
});