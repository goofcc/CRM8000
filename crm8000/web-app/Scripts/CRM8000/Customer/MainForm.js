﻿Ext.define("CRM8000.Customer.MainForm", {
    extend: "Ext.panel.Panel",

    border: 0,
    layout: "border",

    initComponent: function () {
        var me = this;
        Ext.define("CRM8000CustomerCategory", {
            extend: "Ext.data.Model",
            fields: ["id", "code", "name"]
        });

        var categoryGrid = Ext.create("Ext.grid.Panel", {
            title: "客户分类",
            forceFit: true,
            columnLines: true,
            columns: [
                { header: "类别编码", dataIndex: "code", flex: 1, menuDisabled: true, sortable: false },
                { header: "类别", dataIndex: "name", flex: 1, menuDisabled: true, sortable: false }
            ],
            store: Ext.create("Ext.data.Store", {
                model: "CRM8000CustomerCategory",
                autoLoad: false,
                data: []
            }),
            listeners: {
                select: {
                    fn: me.onCategoryGridSelect,
                    scope: me
                },
                itemdblclick: {
                    fn: me.onEditCategory,
                    scope: me
                }
            }
        });
        me.categoryGrid = categoryGrid;

        Ext.define("CRM8000Customer", {
            extend: "Ext.data.Model",
            fields: ["id", "code", "name", "tel", "qq", "categoryId"]
        });

        var customerGrid = Ext.create("Ext.grid.Panel", {
            title: "客户列表",
            columnLines: true,
            columns: [
                { header: "客户编码", dataIndex: "code", menuDisabled: true, sortable: false },
                { header: "客户名称", dataIndex: "name", menuDisabled: true, sortable: false, width: 300 },
                { header: "联系电话", dataIndex: "tel", menuDisabled: true, sortable: false },
                { header: "QQ", dataIndex: "qq", menuDisabled: true, sortable: false }
            ],
            store: Ext.create("Ext.data.Store", {
                autoLoad: false,
                model: "CRM8000Customer",
                data: []
            }),
            listeners: {
                itemdblclick: {
                    fn: me.onEditCustomer,
                    scope: me
                }
            }
        });

        me.customerGrid = customerGrid;

        Ext.apply(me, {
            tbar: [
                    { text: "新增客户分类", iconCls: "CRM8000-button-add", handler: me.onAddCategory, scope: me },
                    { text: "编辑客户分类", iconCls: "CRM8000-button-edit", handler: me.onEditCategory, scope: me },
                    { text: "删除客户分类", iconCls: "CRM8000-button-delete", handler: me.onDeleteCategory, scope: me }, "-",
                    { text: "新增客户", iconCls: "CRM8000-button-add-detail", handler: me.onAddCustomer, scope: me },
                    { text: "修改客户", iconCls: "CRM8000-button-edit-detail", handler: me.onEditCustomer, scope: me },
                    { text: "删除客户", iconCls: "CRM8000-button-delete-detail", handler: me.onDeleteCustomer, scope: me }, "-",
                    {
                        text: "关闭", iconCls: "CRM8000-button-exit", handler: function () {
                            location.replace(CRM8000.Const.BASE_URL);
                        }
                    }
            ],
            items: [
                {
                    region: "center", xtype: "panel", layout: "fit", border: 0,
                    items: [customerGrid]
                },
                {
                    xtype: "panel",
                    region: "west",
                    layout: "fit",
                    width: 300,
                    minWidth: 200,
                    maxWidth: 350,
                    split: true,
                    border: 0,
                    items: [categoryGrid]
                }
            ]
        });

        me.callParent(arguments);

        me.freshCategoryGrid();
    },

    onAddCategory: function () {
        var form = Ext.create("CRM8000.Customer.CategoryEditForm", {
            parentForm: this
        });

        form.show();
    },

    onEditCategory: function () {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要编辑的客户分类");
            return;
        }

        var category = item[0];

        var form = Ext.create("CRM8000.Customer.CategoryEditForm", {
            parentForm: this,
            entity: category
        });

        form.show();
    },

    onDeleteCategory: function () {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要删除的客户分类");
            return;
        }

        var category = item[0];
        var info = "请确认是否删除客户分类: <span style='color:red'>" + category.get("name") + "</span>";
        var me = this;
        CRM8000.MsgBox.confirm(info, function () {
            var el = Ext.getBody();
            el.mask("正在删除中...");
            Ext.Ajax.request({
                url: CRM8000.Const.BASE_URL + "customer/deleteCategory",
                method: "POST",
                params: { id: category.get("id") },
                callback: function (options, success, response) {
                    el.unmask();

                    if (success) {
                        var data = Ext.JSON.decode(response.responseText);
                        if (data.success) {
                            CRM8000.MsgBox.showInfo("成功完成删除操作", function () {
                                me.freshCategoryGrid();
                            });
                        } else {
                            CRM8000.MsgBox.showInfo(data.msg);
                        }
                    } else {
                        CRM8000.MsgBox.showInfo("网络错误", function () {
                            window.location.reload();
                        });
                    }
                }

            });
        });
    },

    freshCategoryGrid: function (id) {
        var grid = this.categoryGrid;
        var el = grid.getEl() || Ext.getBody();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "customer/categoryList",
            method: "POST",
            callback: function (options, success, response) {
                var store = grid.getStore();

                store.removeAll();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    store.add(data);

                    if (store.getCount() > 0) {
                        if (id) {
                            var r = store.findExact("id", id);
                            if (r != -1) {
                                grid.getSelectionModel().select(r);
                            }
                        } else {
                            grid.getSelectionModel().select(0);
                        }
                    }
                }

                el.unmask();
            }
        });
    },

    freshCustomerGrid: function (id) {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            return;
        }

        var category = item[0];

        var grid = this.customerGrid;
        grid.setTitle("属于分类 [" + category.get("name") + "] 的客户");

        var el = grid.getEl() || Ext.getBody();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "customer/customerList",
            params: { id: category.get("id") },
            method: "POST",
            callback: function (options, success, response) {
                var store = grid.getStore();

                store.removeAll();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);

                    grid.setTitle("属于分类 [" + category.get("name") + "] 的客户" + ", 共计 "
                        + data.length + " 个");
                    store.add(data);

                    if (store.getCount() > 0) {
                        if (id) {
                            var r = store.findExact("id", id);
                            if (r != -1) {
                                grid.getSelectionModel().select(r);
                            }
                        } else {
                            grid.getSelectionModel().select(0);
                        }
                    }
                }

                el.unmask();
            }
        });
    },
    // private
    onCategoryGridSelect: function () {
        this.freshCustomerGrid();
    },

    onAddCustomer: function () {
        if (this.categoryGrid.getStore().getCount() == 0) {
            CRM8000.MsgBox.showInfo("没有客户分类，请先新增客户分类");
            return;
        }

        var form = Ext.create("CRM8000.Customer.CustomerEditForm", {
            parentForm: this
        });

        form.show();
    },

    onEditCustomer: function () {
        var item = this.categoryGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("没有选择客户分类");
            return;
        }
        var category = item[0];

        var item = this.customerGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要编辑的客户");
            return;
        }

        var customer = item[0];
        customer.set("categoryId", category.get("id"));
        var form = Ext.create("CRM8000.Customer.CustomerEditForm", {
            parentForm: this,
            entity: customer
        });

        form.show();
    },

    onDeleteCustomer: function () {
        var item = this.customerGrid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要删除的客户");
            return;
        }

        var customer = item[0];

        var info = "请确认是否删除客户: <span style='color:red'>" + customer.get("name") + "</span>";
        var me = this;
        CRM8000.MsgBox.confirm(info, function () {
            var el = Ext.getBody();
            el.mask("正在删除中...");
            Ext.Ajax.request({
                url: CRM8000.Const.BASE_URL + "customer/deleteCustomer",
                method: "POST",
                params: { id: customer.get("id") },
                callback: function (options, success, response) {
                    el.unmask();

                    if (success) {
                        var data = Ext.JSON.decode(response.responseText);
                        if (data.success) {
                            CRM8000.MsgBox.showInfo("成功完成删除操作", function () {
                                me.freshCustomerGrid();
                            });
                        } else {
                            CRM8000.MsgBox.showInfo(data.msg);
                        }
                    }
                }

            });
        });
    }
});