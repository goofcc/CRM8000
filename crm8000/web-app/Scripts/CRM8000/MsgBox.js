﻿Ext.define("CRM8000.MsgBox", {
    statics: {
        showInfo: function (info, func) {
            Ext.Msg.show({
                title: "提示",
                msg: info,
                icon: Ext.Msg.INFO,
                buttons: Ext.Msg.OK,
                modal: true,
                fn: function () {
                    if (func) {
                        func();
                    }
                }
            });
        },

        confirm: function (confirmInfo, funcOnYes) {
            Ext.Msg.show({
                title: "提示",
                msg: confirmInfo,
                icon: Ext.Msg.QUESTION,
                buttons: Ext.Msg.YESNO,
                modal: true,
                fn: function (id, msg) {
                    if (id == "yes" && funcOnYes) {
                        funcOnYes();
                    }
                }
            });
        }
    }
});