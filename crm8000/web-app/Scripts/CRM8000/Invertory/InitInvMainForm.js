﻿Ext.define("CRM8000.Invertory.InitInvMainForm", {
    extend: "Ext.panel.Panel",

    initComponent: function () {
        var me = this;

        Ext.define("CRM8000InitInv", {
            extend: "Ext.data.Model",
            fields: ["id", "goodsCode", "goodsName", "goodsSpec", "goodsCount", "goodsUnit", "goodsMoney", "goodsPrice", "initDate"]
        });
        var storePWBill = Ext.create("Ext.data.Store", {
            autoLoad: false,
            model: "CRM8000InitInv",
            data: []
        });

        var gridInitInv = Ext.create("Ext.grid.Panel", {
            title: "请选择一个仓库",
            columnLines: true,
            columns: [
                { header: "商品编码", dataIndex: "goodsCode", menuDisabled: true, sortable: false },
                { header: "品名", dataIndex: "goodsName", menuDisabled: true, sortable: false, width: 300 },
                { header: "规格型号", dataIndex: "goodsSpec", menuDisabled: true, sortable: false, width: 200 },
                { header: "期初数量", dataIndex: "goodsCount", menuDisabled: true, sortable: false, align: "right" },
                { header: "单位", dataIndex: "goodsUnit", menuDisabled: true, sortable: false, width: 50 },
                { header: "期初金额", dataIndex: "goodsMoney", menuDisabled: true, sortable: false, align: "right", xtype: "numbercolumn" },
                { header: "期初单价", dataIndex: "goodsPrice", menuDisabled: true, sortable: false, align: "right", xtype: "numbercolumn" },
                { header: "建账日期", dataIndex: "initDate", menuDisabled: true, sortable: false, width: 80 }
            ],
            store: storePWBill
        });
        this.gridInitInv = gridInitInv;

        Ext.define("CRM8000Warehouse", {
            extend: "Ext.data.Model",
            fields: ["id", "code", "name", "inited"]
        });

        var gridWarehouse = Ext.create("Ext.grid.Panel", {
            title: "仓库",
            forceFit: true,
            columnLines: true,
            columns: [
                { header: "仓库编码", dataIndex: "code", menuDisabled: true, sortable: false, width: 60 },
                { header: "仓库名称", dataIndex: "name", flex: 1, menuDisabled: true, sortable: false },
                { header: "建账完毕", dataIndex: "inited", menuDisabled: true, sortable: false, width: 60,
                	renderer: function(value) {
                		return value ? "完毕" : "<span style='color:red'>未完</span>";
                	}}
            ],
            store: Ext.create("Ext.data.Store", {
                model: "CRM8000Warehouse",
                autoLoad: false,
                data: []
            }),
            listeners: {
                select: {
                    fn: me.onWarehouseGridSelect,
                    scope: me
                }
            }
        });
        me.gridWarehouse = gridWarehouse;

        Ext.apply(me, {
            border: 0,
            layout: "border",
            tbar: [{
                text: "建账", iconCls: "CRM8000-button-add", scope: me, handler: me.onInitInv
            }, "-", {
                text: "刷新", iconCls: "CRM8000-button-refresh", scope: me, handler: function () {
                    me.freshInvGrid();
                }
            }, "-", 
            {
            	text: "标记建账完毕", iconCls: "CRM8000-button-commit", scope: me, handler: me.onFinish
            },
            {
            	text: "取消建账完毕", iconCls: "CRM8000-button-cancel", scope: me, handler: me.onCancel
            },
            "-",{
                text: "关闭", iconCls: "CRM8000-button-exit", handler: function () {
                    location.replace(CRM8000.Const.BASE_URL);
                }
            }],
            items: [
                {
                    region: "west", xtype: "panel", layout: "fit", border: 0,
                    width: 300,
                    minWidth: 200,
                    split: true,
                    items: [gridWarehouse]
                }, {
                    region: "center", xtype: "panel", layout: "fit", border: 0,
                    items: [gridInitInv]
                }
            ]
        });

        me.callParent(arguments);

        me.freshWarehouseGrid();
    },

    freshWarehouseGrid: function () {
        var grid = this.gridWarehouse;
        var el = grid.getEl() || Ext.getBody();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "initInvertory/warehouseList",
            method: "POST",
            callback: function (options, success, response) {
                var store = grid.getStore();

                store.removeAll();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    store.add(data);
                }

                el.unmask();
            }
        });
    },

    onWarehouseGridSelect: function () {
        this.freshInvGrid();
    },

    freshInvGrid: function () {
        var item = this.gridWarehouse.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            return;
        }

        var warehouse = item[0];

        var grid = this.gridInitInv;
        grid.setTitle("仓库: " + warehouse.get("name"));

        var el = grid.getEl();
        el.mask(CRM8000.Const.LOADING);
        Ext.Ajax.request({
            url: CRM8000.Const.BASE_URL + "initInvertory/initInfoList",
            params: { warehouseId: warehouse.get("id") },
            method: "POST",
            callback: function (options, success, response) {
                var store = grid.getStore();

                store.removeAll();

                if (success) {
                    var data = Ext.JSON.decode(response.responseText);
                    store.add(data);

                    if (store.getCount() > 0) {
                        grid.getSelectionModel().select(0);
                    }
                }

                el.unmask();
            }
        });
    },

    onInitInv: function () {
        var item = this.gridWarehouse.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要建账的仓库");
            return;
        }
        var warehouse = item[0];
        
        if (warehouse.get("inited")) {
        	CRM8000.MsgBox.showInfo("仓库[" + warehouse.get("name") + "]已经建账完毕");
        	return;
        }

        var form = Ext.create("CRM8000.Invertory.InitInvEditForm", {
            warehouse: warehouse
        });
        form.show();
    },
    
    onFinish: function() {
    	var me = this;
    	var item = this.gridWarehouse.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要标记的仓库");
            return;
        }
        var warehouse = item[0];
        if (warehouse.get("inited")) {
        	CRM8000.MsgBox.showInfo("仓库[" + warehouse.get("name") + "]已经标记建账完毕");
        	return;
        }
        
        CRM8000.MsgBox.confirm("请确认是否给仓库[" + warehouse.get("name") + "]标记建账完毕?", function(){
            var el = Ext.getBody();
            el.mask(CRM8000.Const.SAVING);
            Ext.Ajax.request({
                url: CRM8000.Const.BASE_URL + "initInvertory/finish",
                params: { warehouseId: warehouse.get("id") },
                method: "POST",
                callback: function (options, success, response) {
                    el.unmask();
                    if (success) {
                        var data = Ext.JSON.decode(response.responseText);
                        if (data.success) {
                            CRM8000.MsgBox.showInfo("成功标记建账完毕", function () {
                                me.freshWarehouseGrid();
                            });
                        } else {
                            CRM8000.MsgBox.showInfo(data.msg);
                        }
                    } else {
                        CRM8000.MsgBox.showInfo("网络错误");
                    }
                }
            });
        });
    },
    
    onCancel: function() {
    	var me = this;
    	var item = this.gridWarehouse.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
            CRM8000.MsgBox.showInfo("请选择要取消建账的仓库");
            return;
        }
        var warehouse = item[0];
    	
        if (!warehouse.get("inited")) {
        	CRM8000.MsgBox.showInfo("仓库[" + warehouse.get("name") + "]没有标记建账完毕");
        	return;
        }
        
        CRM8000.MsgBox.confirm("请确认是否取消仓库[" + warehouse.get("name") + "]的建账完毕标志?", function(){
        	var el = Ext.getBody();
            el.mask(CRM8000.Const.SAVING);
            Ext.Ajax.request({
                url: CRM8000.Const.BASE_URL + "initInvertory/cancel",
                params: { warehouseId: warehouse.get("id") },
                method: "POST",
                callback: function (options, success, response) {
                    el.unmask();
                    if (success) {
                        var data = Ext.JSON.decode(response.responseText);
                        if (data.success) {
                            CRM8000.MsgBox.showInfo("成功取消建账标志", function () {
                                me.freshWarehouseGrid();
                            });
                        } else {
                            CRM8000.MsgBox.showInfo(data.msg);
                        }
                    } else {
                        CRM8000.MsgBox.showInfo("网络错误");
                    }
                }
            });
        });
    }
});