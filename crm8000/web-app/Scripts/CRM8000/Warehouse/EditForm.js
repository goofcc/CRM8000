﻿Ext.define("CRM8000.Warehouse.EditForm", {
    extend: "Ext.window.Window",

    config: {
        parentForm: null,
        entity: null
    },

    initComponent: function () {
        var me = this;

        var entity = me.getEntity();

        this.adding = entity == null;

        var buttons = [];
        if (!entity) {
            buttons.push({
                text: "保存并继续新增",
                formBind: true,
                handler: function () {
                    me.onOK(true);
                },
                scope: me
            });
        }

        buttons.push({
            text: "保存",
            formBind: true,
            iconCls: "CRM8000-button-ok",
            handler: function () {
                me.onOK(false);
            }, scope: me
        }, {
            text: entity == null? "关闭" : "取消", handler: function () {
                me.close();
            }, scope: me
        });

        Ext.apply(me, {
            title: entity == null ? "新增仓库" : "编辑仓库",
            modal: true,
            onEsc: Ext.emptyFn,
            width: 400,
            height: 130,
            layout: "fit",
            defaultFocus: "editCode",
            items: [
                {
                    id: "editForm",
                    xtype: "form",
                    layout: "form",
                    height: "100%",
                    bodyPadding: 5,
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelWidth: 60,
                        labelAlign: "right",
                        labelSeparator: "",
                        msgTarget: 'side'
                    },
                    items: [
                        {
                            xtype: "hidden",
                            name: "id",
                            value: entity == null ? null : entity.get("id")
                        }, {
                            id: "editCode",
                            fieldLabel: "仓库编码",
                            allowBlank: false,
                            blankText: "没有输入仓库编码",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            name: "code",
                            value: entity == null ? null : entity.get("code"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditCodeSpecialKey,
                                    scope: me
                                }
                            }
                        }, {
                            id: "editName",
                            fieldLabel: "仓库名称",
                            allowBlank: false,
                            blankText: "没有输入仓库名称",
                            beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                            name: "name",
                            value: entity == null ? null : entity.get("name"),
                            listeners: {
                                specialkey: {
                                    fn: me.onEditNameSpecialKey,
                                    scope: me
                                }
                            }
                        }
                    ],
                    buttons: buttons
                }
            ]
        });

        me.callParent(arguments);
    },

    // private
    onOK: function (thenAdd) {
        var me = this;
        var f = Ext.getCmp("editForm");
        var el = f.getEl();
        el.mask(CRM8000.Const.SAVING);
        f.submit({
            url: CRM8000.Const.BASE_URL + "warehouse/editWarehouse",
            method: "POST",
            success: function (form, action) {
                el.unmask();
                if (thenAdd) {
                    me.getParentForm().freshGrid(action.result.id);

                    me.clearEdit();
                } else {
                    CRM8000.MsgBox.showInfo("数据保存成功", function () {
                        me.close();
                        me.getParentForm().freshGrid(action.result.id);
                    });
                }
            },
            failure: function (form, action) {
                el.unmask();
                CRM8000.MsgBox.showInfo(action.result.msg, function () {
                    Ext.getCmp("editCode").focus();
                });
            }
        });
    },

    onEditCodeSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            Ext.getCmp("editName").focus();
        }
    },

    onEditNameSpecialKey: function (field, e) {
        if (e.getKey() == e.ENTER) {
            var f = Ext.getCmp("editForm");
            if (f.getForm().isValid()) {
                this.onOK(this.adding);
            }
        }
    },

    // private
    clearEdit: function () {
        Ext.getCmp("editCode").focus();

        var editors = [Ext.getCmp("editCode"), Ext.getCmp("editName")];
        for (var i = 0; i < editors.length; i++) {
            var edit = editors[i];
            edit.setValue(null);
            edit.clearInvalid();
        }
    }
});