﻿Ext.define("CRM8000.User.LoginForm", {
    extend: 'Ext.window.Window',

    config:  {
    	baseURL: ""
    },
    
    header: {
        title: "<span style='font-size:120%'>登录 - CRM8000</span>",
        iconCls: "CRM8000-login",
        height: 40
    },
    modal: true,
    closable: false,
    onEsc: Ext.emptyFn,
    width: 400,
    height: 140,
    layout: "fit",
    defaultFocus: Ext.util.Cookies.get("CRM8000_user_login_name") ? "editPassword" : "editLoginName",

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            items: [{
                id: "loginForm",
                xtype: "form",
                layout: "form",
                height: "100%",
                border: 0,
                bodyPadding: 5,
                defaultType: 'textfield',
                fieldDefaults: {
                    labelWidth: 60,
                    labelAlign: "right",
                    labelSeparator: "",
                    msgTarget: 'side'
                },
                items: [{
                    id: "editLoginName",
                    fieldLabel: "用户名",
                    allowBlank: false,
                    blankText: "没有输入用户名",
                    beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                    name: "loginName",
                    value: Ext.util.Cookies.get("CRM8000_user_login_name"),
                    listeners: {
                        specialkey: function (field, e) {
                            if (e.getKey() == e.ENTER) {
                                Ext.getCmp("editPassword").focus();
                            }
                        }
                    }
                }, {
                    id: "editPassword",
                    fieldLabel: "密码",
                    allowBlank: false,
                    blankText: "没有输入密码",
                    beforeLabelTextTpl: CRM8000.Const.REQUIRED,
                    inputType: "password",
                    name: "password",
                    listeners: {
                        specialkey: function (field, e) {
                            if (e.getKey() == e.ENTER) {
                                if (Ext.getCmp("loginForm").getForm().isValid()) {
                                    me.onOK();
                                }
                            }
                        }
                    }
                }],
                buttons: [{
                    text: "登录",
                    formBind: true,
                    handler: me.onOK,
                    iconCls: "CRM8000-button-ok"
                }]
            }]
        });

        me.callParent(arguments);
    },

    getPostURL: function() {
    	return this.getBaseURL() + "user/loginPOST";
    },
    
    // private
    onOK: function () {
        var me = this;

        var loginName = Ext.getCmp("editLoginName").getValue();
        var f = Ext.getCmp("loginForm");
        var el = f.getEl() || Ext.getBody();
        el.mask("系统登录中...");
        f.getForm().submit({
            url: me.getPostURL(),
            method: "POST",
            success: function (form, action) {
                Ext.util.Cookies.set("CRM8000_user_login_name", encodeURIComponent(loginName),
                    Ext.Date.add(new Date(), Ext.Date.YEAR, 1));

                location.replace(me.getBaseURL());
            },
            failure: function (form, action) {
                el.unmask();
                CRM8000.MsgBox.showInfo(action.result.msg, function () {
                    var editPassword = Ext.getCmp("editPassword");
                    editPassword.setValue(null);
                    editPassword.clearInvalid();
                    editPassword.focus();
                });
            }
        });
    }
});