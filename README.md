关于CRM8000
-------------
>CRM8000是面向小型企业的客户关系管理系统，同时也包括进销存系统。

>本项目仅以学习Grails为目的。
>从2015-2-27起，本项目不再做任何维护和升级，
>如果您有商用需求，欢迎访问同门产品PSI：http://git.oschina.net/crm8000/PSI

CRM8000的开源协议
-------------
>CRM8000的开源协议为GPL v3

>如果您有Sencha Ext JS的商业许可（参见： http://www.sencha.com/products/extjs/licensing/ ），那么CRM8000的开源协议为BSD。

>GPL v3协议下，您必须保持CRM8000代码的开源。 
>BSD协议下，您可以闭源并私有化CRM8000的代码，作为您自己的商业产品来销售。